#version 330

//smooth in vec4 f_Color;
//flat in vec4 f_Color;
in vec3 f_Color;

in vec2 TexCoord;

out vec4 outputColor;

uniform sampler2D outputTexture;
uniform bool useTextures;

void main()
{
    if (useTextures)
        outputColor = texture2D(outputTexture, TexCoord);// * vec4(f_Color, 1.0f);
    else
        outputColor = vec4(f_Color, 1.0f);
    //outputColor = texture2D(outputTexture, TexCoord) * f_Color;// + f_Color;
    //outputColor = texture2D(outputTexture, TexCoord);
}
