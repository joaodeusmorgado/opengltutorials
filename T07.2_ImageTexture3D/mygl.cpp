#include "mygl.h"
#include <QResource>
#include <QFile>
#include <QDebug>
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <sstream>
#include <QMatrix4x4>

using namespace std;


MyGL::MyGL() :
    gl_texture_n(0),
    scale(1),
    xRot(0),
    yRot(0),
    zRot(0)
{
    //to use a texture
    //1 - glGenTextures — generate texture names
    //void glGenTextures( GLsizei n, GLuint * textures);

    //2 - glActiveTexture - Specifies which texture unit to make active
    //void glActiveTexture(GLenum texture​);
    //glActiveTexture(GL_TEXTURE0);
    //glActiveTexture(GL_TEXTURE1); or glActiveTexture(GL_TEXTURE0+i);

    //3 - glBindTexture — bind a named texture to a texturing target
    //void glBindTexture( GLenum target, GLuint texture);

    //4 - specify a two-dimensional texture image, or another dimension
    //void glTexImage2D( GLenum target, GLint level, GLint internalFormat, GLsizei width, GLsizei height,
    //                  GLint border, GLenum format, GLenum type, const GLvoid * data);

    //5 - glTexParameter, glTextureParameter — set texture parameters
    //void glTexParameteri(	GLenum target, GLenum pname, GLint param);

}

void MyGL::setTexParam(QString value)
{
    if (value == m_texParam)
        return;
    m_texParam = value;
    emit texParamChanged(value);
}

void MyGL::initializeVertexBuffer()
{

    // 3d axis --------------

    float range = 2.0f;


    const float vertexData[] = {
        -range, 0.0f,    0.0f,   //axis x-
        1.0f,   0.0f,    0.0f,   //color
        range,  0.0f,    0.0f,   //axis x+
        1.0f,   0.0f,    0.0f,   //color

        0.0f,   -range,  0.0f,   //axis y-
        0.0f,   1.0f,    0.0f,   //color
        0.0f,   range,   0.0f,   //axis y+
        1.0f,   1.0f,    0.0f,   //color

        0.0f,   0.0f,    -range, //axis z-
        0.0f,   0.0f,    1.0f,   //color
        0.0f,   0.0f,    range,  //axis z+
        0.0f,   0.0f,    1.0f    //color
    };


    glGenBuffers(1, &positionBufferObject);//create a buffer and save it on the positionBufferObject handle
    glBindBuffer(GL_ARRAY_BUFFER, positionBufferObject); // bind the buffer
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW); //save data to the buffer
    glBindBuffer(GL_ARRAY_BUFFER, 0); //unbinding


    // texture

    QImage myImage = QImage(QString(":/images/images/earth-1024x512.png")).mirrored();
    //QImage myImage = QImage(QString(":/images/images/sunset1.png")).mirrored();
    myImage = myImage.convertToFormat(QImage::Format_ARGB32);

    glGenTextures(1, &textureID);//create a texture ID handle

    glActiveTexture(GL_TEXTURE0+gl_texture_n);
    glBindTexture(GL_TEXTURE_2D, textureID); // glBindTexture — bind a named texture to a texturing target
    // generate the texture with the image data


    border = 0;
    imgWidth = myImage.width();
    imgHeight = myImage.height();


    //specify a two-dimensional texture image
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, imgWidth, imgHeight, border,
                 GL_BGRA, GL_UNSIGNED_BYTE, myImage.bits());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glGenerateMipmap(GL_TEXTURE_2D);


    glBindTexture(GL_TEXTURE_2D, 0);//unbind

}



void MyGL::createShaders()
{

    //vertex shader----------------------------------------

    QFile fileVertex(":/shaders/shaders/vertshader.vert");
    if (!fileVertex.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"file read fail";
    }
    else
        qDebug()<<"file read ok";

    QTextStream in(&fileVertex);
    QString strShaderVertFile = in.readAll();
    qDebug()<<"strShaderVertFile: "<<strShaderVertFile;
    fileVertex.close();

    shaderVert = glCreateShader(GL_VERTEX_SHADER);

    //const char *strFileData = strShaderFile.toStdString().c_str();//wrong ??? http://stackoverflow.com/questions/11138705/is-there-anything-wrong-with-my-glsl-shader-loading-code
    //glShaderSource(shaderVert, 1, &strFileData, NULL);

    std::string strFileData = strShaderVertFile.toStdString();
    const GLchar *data = reinterpret_cast<const GLchar*>(strFileData.c_str());
    glShaderSource(shaderVert, 1, &data, NULL);

    glCompileShader(shaderVert);

    GLint status;
    glGetShaderiv(shaderVert, GL_COMPILE_STATUS, &status);

    if (status == GL_FALSE)
    {
        qDebug()<<"vertex shader compile fail";
        GLint infoLogLength;
        glGetShaderiv(shaderVert, GL_INFO_LOG_LENGTH, &infoLogLength);
        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetShaderInfoLog(shaderVert, infoLogLength, NULL, strInfoLog);
        const char *strShaderType = "vertex";
        fprintf(stderr, "Compile failure in %s shader:\n%s\n", strShaderType, strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"vertex shader compile ok";
        fprintf(stderr, "Compile vertex shader ok");
    }


    //fragment shader----------------------------------------

    QFile fileFrag(":/shaders/shaders/fragshader.frag");
    if (!fileFrag.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"file read fail";
    }
    else
        qDebug()<<"file read ok";

    QTextStream in2(&fileFrag);
    QString strShaderFragFile = in2.readAll();
    qDebug()<<"strShaderFragFile: "<<strShaderFragFile;
    fileFrag.close();

    shaderFrag = glCreateShader(GL_FRAGMENT_SHADER);

    std::string strFileFragData = strShaderFragFile.toStdString();
    const GLchar *dataFrag = reinterpret_cast<const GLchar*>(strFileFragData.c_str());

    glShaderSource(shaderFrag, 1, &dataFrag, NULL);
    glCompileShader(shaderFrag);

    //GLint status;
    glGetShaderiv(shaderFrag, GL_COMPILE_STATUS, &status);

    if (status == GL_FALSE)
    {
        qDebug()<<"fragment shader compile fail";
        GLint infoLogLength;
        glGetShaderiv(shaderFrag, GL_INFO_LOG_LENGTH, &infoLogLength);
        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetShaderInfoLog(shaderFrag, infoLogLength, NULL, strInfoLog);
        const char *strShaderFragType = "fragment";
        fprintf(stderr, "Compile failure in %s shader:\n%s\n", strShaderFragType, strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"fragment shader compile ok";
        fprintf(stderr, "Compile fragment shader ok");
    }

    //shader program---------------------------------------------------------
    program = glCreateProgram();

    glAttachShader(program, shaderVert);
    glAttachShader(program, shaderFrag);

    glLinkProgram(program);

//    GLint status;
    glGetProgramiv (program, GL_LINK_STATUS, &status);
    if (status == GL_FALSE)
    {
        qDebug()<<"program shader link failled";
        GLint infoLogLength;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);

        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetProgramInfoLog(program, infoLogLength, NULL, strInfoLog);
        fprintf(stderr, "Linker failure: %s\n", strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"program shader link ok";
    }


    glDetachShader(program, shaderVert);
    glDetachShader(program, shaderFrag);


//    return program;
}

void MyGL::initializeGL()
{
    initializeOpenGLFunctions();

    qDebug()<<"initializeGL";
    createShaders();

    //create triangle and buffer data
    initializeVertexBuffer();

    glEnable(GL_DEPTH_TEST);

    glClearColor(0.7f, 0.7f, 0.7f, 0.0f);
    //glClearColor(0.0f, 0.0f, 1.0f, 0.0f);
}

void MyGL::resizeGL(int w, int h)
{
    glViewport(0, 0, (GLsizei)w, (GLsizei)h);

    float Range = 2;
    float RangeX = Range;
    float RangeY = Range;
    float RangeZ = Range;

    projection.setToIdentity();
    if (w <= h)
        projection.ortho(-(RangeX),RangeX,-RangeY*h/w,RangeY*h/w,-(RangeZ*2),RangeZ*2);
    else
        projection.ortho(-(RangeX*w/h),RangeX*w/h,-RangeY,RangeY,-(RangeZ*2),RangeZ*2);
}

void MyGL::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(program);

    view.setToIdentity();
    view.rotate(xRot, 1.0f, 0.0f, 0.0f );
    view.rotate(yRot, 0.0f, 1.0f, 0.0f );
    view.rotate(zRot, 0.0f, 0.0f, 1.0f );
    view.scale(scale, scale, scale);

    mvp = projection * view;
    glUniformMatrix4fv(glGetUniformLocation(program , "mvp"), 1, false, mvp.constData());

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);




    // texture---------------------------------

    //this will add a rgb tone to the image
    GLfloat r = 0.0f;
    GLfloat g = 1.0f;
    GLfloat b = 1.0f;

    GLfloat posX = 4;
    GLfloat posY = 2;
    GLfloat posZ = 1;
    GLfloat s = 1.0f;
    GLfloat t = 1.0f;

    /*GLfloat vertices[] = {
        // Positions    // Colors  // Texture Coords
        -posX, -posY, posZ/2,  r, g, b,   0.0f, 0.0f,   // Bottom Left
        -posX,  posY, posZ/2,  r, g, b,   0.0f, st,    // Top Left
        posX, -posY, posZ/2,   r, g, b,   st, 0.0f,   // Bottom Right
        posX,  posY, posZ/2,   r, g, b,   st, st,   // Top Right
    };*/

    GLfloat vertices[] = {
        // Positions    // Colors  // Texture Coords st
        -posX, -posY, posZ/2,  r, g, b,   0.0f, 0.0f,   // Bottom Left
        -posX,  posY, posZ/2,  r, g, b,   0.0f, t,    // Top Left

        0, -posY, posZ/2,  r, g, b,   s/2, 0.0f,   // Bottom Center
        0,  posY, posZ/2,  r, g, b,   s/2, t,    // Top Center

        0, -posY, -posZ,   r, g, b,   s, 0.0f,   // Bottom Right
        0,  posY, -posZ,   r, g, b,   s, t,   // Top Right
    };

    // texture---------------------
    glActiveTexture(GL_TEXTURE0+gl_texture_n);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glUniform1i(glGetUniformLocation(program , "outputTexture"), gl_texture_n);
    glUniform1i(glGetUniformLocation(program , "useTextures"), 1); //true

    glVertexAttribPointer(0, 3, GL_FLOAT,GL_FALSE, 8 * sizeof(GLfloat), &vertices[0]); //positions start at position 0 in vertices array
    glVertexAttribPointer(1, 3, GL_FLOAT,GL_FALSE, 8 * sizeof(GLfloat), &vertices[3]); //colors start at position 3 in vertices array
    glVertexAttribPointer(2, 2, GL_FLOAT,GL_FALSE, 8 * sizeof(GLfloat), &vertices[6]); //textures coordinates start at position 6 in vertices array

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 6);
    //glDrawArrays(GL_LINES, 0, 4);

    glBindTexture(GL_TEXTURE_2D, 0);//unbind
    // texture---------------------


    // axis---------------------
    GLsizei stride = 6*sizeof(float);
    //Stride is the distance from the beginning of one index, to the begginging of the following index
    //8 is the number of elements in one index, 4 vertex position elements + 4 vertex color elements

    glBindBuffer(GL_ARRAY_BUFFER, positionBufferObject);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride, 0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, stride, (void*)(3*sizeof(float))); //4*sizeof(float) => 16
    //The offset 16 is the position where color elements start in the array
    //i.e. 4(the number of floats in the vec4 position) * sizeof(float)

    glUniform1i(glGetUniformLocation(program, "useTextures"), 0); //false
    glDrawArrays(GL_LINES, 0, 6);

    glBindBuffer(GL_ARRAY_BUFFER, 0);//unbind
    // axis---------------------


    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
    glUseProgram(0);//release program

}

/*void MyGL::mousePressEvent(QMouseEvent *event)
{
    Q_UNUSED(event);

    glBindTexture(GL_TEXTURE_2D, textureID); // glBindTexture — bind a named texture to a texturing target

    wrap++;
    if (wrap > 4)
        wrap = 0;

    setupTexture();

    glBindTexture(GL_TEXTURE_2D, 0);//unbind

    update();

}
*/
