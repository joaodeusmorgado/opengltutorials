#include "mygl.h"
#include <QResource>
#include <QFile>
#include <QDebug>
#include <time.h>
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <sstream>
using namespace std;


MyGL::MyGL()
{

}

void MyGL::initializeVertexBuffer()
{
    //triangle


    glGenBuffers(1, &vbo);//create 1 buffer and save it on the vbo handle


    //triangle data, first is stored the vertex data, after that is stored the color data

    float delta = 0.0f;
    const float triangleArray[] = {
        0.0f+delta,    0.5f+delta, 0.0f, 1.0f,//position
        0.5f+delta, -0.366f+delta, 0.0f, 1.0f,//position
        -0.5f+delta, -0.366f+delta, 0.0f, 1.0f,//position
        1.0f,    0.5f, 0.0f, 1.0f,//color
        1.0f,    0.0f, 0.0f, 1.0f,//color
        1.0f,    1.0f, 0.0f, 1.0f//color
    };


    glBindBuffer(GL_ARRAY_BUFFER, vbo); // bind the buffer
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleArray), triangleArray, GL_STATIC_DRAW); //save data to the buffer
    glBindBuffer(GL_ARRAY_BUFFER, 0); //unbinding


}

void MyGL::initializeVao()
{

    //create 1 vao - vertex array object
    glGenVertexArrays(1, &vao);

    //bind the vao
    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*)48); //12*sizeof(float) => 48
    //48 is the offset position where the color data starts, in the triangle array
    glBindBuffer(GL_ARRAY_BUFFER, 0); //unbind buffer

    glBindVertexArray(0);//unbind vao

}

void MyGL::createShaders()
{

    //vertex shader----------------------------------------

    QFile fileVertex(":/shaders/shaders/vertshader.vert");
    if (!fileVertex.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"file read fail";
    }
    else
        qDebug()<<"file read ok";

    QTextStream in(&fileVertex);
    QString strShaderVertFile = in.readAll();
    qDebug()<<"strShaderVertFile: "<<strShaderVertFile;
    fileVertex.close();

    shaderVert = glCreateShader(GL_VERTEX_SHADER);

    //const char *strFileData = strShaderFile.toStdString().c_str();//wrong ??? http://stackoverflow.com/questions/11138705/is-there-anything-wrong-with-my-glsl-shader-loading-code
    //glShaderSource(shaderVert, 1, &strFileData, NULL);

    std::string strFileData = strShaderVertFile.toStdString();
    const GLchar *data = reinterpret_cast<const GLchar*>(strFileData.c_str());
    glShaderSource(shaderVert, 1, &data, NULL);

    glCompileShader(shaderVert);

    GLint status;
    glGetShaderiv(shaderVert, GL_COMPILE_STATUS, &status);

    if (status == GL_FALSE)
    {
        qDebug()<<"vertex shader compile fail";
        GLint infoLogLength;
        glGetShaderiv(shaderVert, GL_INFO_LOG_LENGTH, &infoLogLength);
        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetShaderInfoLog(shaderVert, infoLogLength, NULL, strInfoLog);
        const char *strShaderType = "vertex";
        fprintf(stderr, "Compile failure in %s shader:\n%s\n", strShaderType, strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"vertex shader compile ok";
        fprintf(stderr, "Compile vertex shader ok");
    }


    //fragment shader----------------------------------------

    QFile fileFrag(":/shaders/shaders/fragshader.frag");
    if (!fileFrag.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"file read fail";
    }
    else
        qDebug()<<"file read ok";

    QTextStream in2(&fileFrag);
    QString strShaderFragFile = in2.readAll();
    qDebug()<<"strShaderFragFile: "<<strShaderFragFile;
    fileFrag.close();

    shaderFrag = glCreateShader(GL_FRAGMENT_SHADER);

    std::string strFileFragData = strShaderFragFile.toStdString();
    const GLchar *dataFrag = reinterpret_cast<const GLchar*>(strFileFragData.c_str());

    glShaderSource(shaderFrag, 1, &dataFrag, NULL);
    glCompileShader(shaderFrag);

    //GLint status;
    glGetShaderiv(shaderFrag, GL_COMPILE_STATUS, &status);

    if (status == GL_FALSE)
    {
        qDebug()<<"fragment shader compile fail";
        GLint infoLogLength;
        glGetShaderiv(shaderFrag, GL_INFO_LOG_LENGTH, &infoLogLength);
        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetShaderInfoLog(shaderFrag, infoLogLength, NULL, strInfoLog);
        const char *strShaderFragType = "fragment";
        fprintf(stderr, "Compile failure in %s shader:\n%s\n", strShaderFragType, strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"fragment shader compile ok";
        fprintf(stderr, "Compile fragment shader ok");
    }

    //shader program---------------------------------------------------------
    program = glCreateProgram();

    glAttachShader(program, shaderVert);
    glAttachShader(program, shaderFrag);

    glLinkProgram(program);

//    GLint status;
    glGetProgramiv (program, GL_LINK_STATUS, &status);
    if (status == GL_FALSE)
    {
        qDebug()<<"program shader link failled";
        GLint infoLogLength;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);

        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetProgramInfoLog(program, infoLogLength, NULL, strInfoLog);
        fprintf(stderr, "Linker failure: %s\n", strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"program shader link ok";
    }


    glDetachShader(program, shaderVert);
    glDetachShader(program, shaderFrag);


//    return program;
}

void MyGL::initializeGL()
{
    initializeOpenGLFunctions();

    qDebug()<<"initializeGL";
    createShaders();

    //create triangle and buffer data
    initializeVertexBuffer();

    initializeVao();

    glClearColor(0.7f, 0.7f, 0.7f, 0.0f);
}

void MyGL::resizeGL(int w, int h)
{
    glViewport(0, 0, (GLsizei)w, (GLsizei)h);
}

void MyGL::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glUseProgram(program);


    glBindVertexArray(vao);//binding a vao will replace calls to
    //glBindBuffer(), glEnableVertexAttribArray() and glVertexAttribPointer()
    //glBindVertexArray() sets the opengl state machine to the state saved in the initializeVao() function

    //update data and draw tringle
    float delta = 0.0f;
    const float triangle1[] = {
        0.0f+delta,    0.5f+delta, 0.0f, 1.0f,//position
        0.5f+delta, -0.366f+delta, 0.0f, 1.0f,//position
        -0.5f+delta, -0.366f+delta, 0.0f, 1.0f,//position
        1.0f,    0.5f, 0.0f, 1.0f,//color
        1.0f,    0.0f, 0.0f, 1.0f,//color
        1.0f,    1.0f, 0.0f, 1.0f//color
    };

    //update data and draw triangle
    updateBufferAllData(vbo, triangle1, sizeof(triangle1));
    glDrawArrays(GL_TRIANGLES, 0, 3);//draw triangle


    delta = -0.5f;
    const float triangle2vertex[] = {
        0.0f+delta,    0.5f+delta, 0.0f, 1.0f,//position
        0.5f+delta, -0.366f+delta, 0.0f, 1.0f,//position
        -0.5f+delta, -0.366f+delta, 0.0f, 1.0f//position
    };
    const float triangle2color[] = {
        1.0f,    0.0f, 0.0f, 1.0f,//red
        0.0f,    1.0f, 0.0f, 1.0f,//green
        0.0f,    0.0f, 1.0f, 1.0f//blue
    };

    //update and draw triangle with the new data
    updateBufferSomeData(vbo, triangle2vertex, sizeof(triangle2vertex), 0);
    updateBufferSomeData(vbo, triangle2color, sizeof(triangle2color), 4*3*sizeof(float));
    glDrawArrays(GL_TRIANGLES, 0, 3);//draw triangle



    delta = 0.5f;
    const float triangle3Vertex[] = {
        0.0f+delta,    0.5f+delta, 0.0f, 1.0f,//position
        0.5f+delta, -0.366f+delta, 0.0f, 1.0f,//position
        -0.5f+delta, -0.366f+delta, 0.0f, 1.0f//position
    };

    updateBufferSomeData(vbo, triangle3Vertex, sizeof(triangle3Vertex), 0);//update only the vertex data
    updateBufferRandomColor(vbo, 4*3*sizeof(float));
    glDrawArrays(GL_TRIANGLES, 0, 3);//draw triangle






    glBindVertexArray(0);// unbing the vao
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glUseProgram(0);//release program

}

void MyGL::updateBufferAllData(GLuint &vbo, const float *data, const size_t &dataSize)
{


    glBindBuffer(GL_ARRAY_BUFFER, vbo); // bind the buffer
    void *ptr = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY); //get a pointer to buffer data store
    memcpy(ptr, data, dataSize); // copy the new date to the buffer

    // or we can also use:
    //float *ptr = (float *)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY); //get a pointer to buffer data store
    //*ptr=*data;

    glUnmapBuffer(GL_ARRAY_BUFFER); //release the mapping to the buffer, invalidating the pointer
    glBindBuffer(GL_ARRAY_BUFFER, 0); //unbinding



    //if we would like to copy data to the buffer byte by byte (probably not efficient),
    // can use glMapBufferRange, like in the following example:

    /*
    glBindBuffer(GL_ARRAY_BUFFER, vbo); // bind the buffer
    float *ptr1 = (float*)glMapBufferRange(GL_ARRAY_BUFFER, 0, dataSize, GL_MAP_WRITE_BIT);
    for(int i=0; i < dataSize/sizeof(float);i++)
    {
        ptr1[i]=data[i];
    }
    glUnmapBuffer(GL_ARRAY_BUFFER); //release the mapping to the buffer, invalidating the pointer
    glBindBuffer(GL_ARRAY_BUFFER, 0); //unbinding
    */


    /*
    // old way to write to buffers, without mapping
    glBindBuffer(GL_ARRAY_BUFFER, vbo); // bind the buffer
    glBufferSubData(GL_ARRAY_BUFFER, 0, dataSize, data);
    glBindBuffer(GL_ARRAY_BUFFER, 0); //unbinding
    */
}

void MyGL::updateBufferSomeData(GLuint vbo, const float *data, const size_t &dataSize, const int &offset)
{

    glBindBuffer(GL_ARRAY_BUFFER, vbo); // bind the buffer
    float *ptr = (float*)glMapBufferRange(GL_ARRAY_BUFFER, offset, dataSize, GL_MAP_WRITE_BIT);

    memcpy(ptr, data, dataSize); // copy the new date to the buffer

    glUnmapBuffer(GL_ARRAY_BUFFER); //release the mapping to the buffer, invalidating the pointer
    glBindBuffer(GL_ARRAY_BUFFER, 0); //unbinding

}

void MyGL::updateBufferRandomColor(GLuint vbo, const int &offset)
{
    glBindBuffer(GL_ARRAY_BUFFER, vbo); // bind the buffer
    float *ptr = (float*)glMapBufferRange(GL_ARRAY_BUFFER, offset, 4*3*sizeof(float), GL_MAP_WRITE_BIT);


    srand(time(NULL));

    int colorSize = 12; //4*3*sizeof(float)/sizeof(float);
    for(int i=0; i < colorSize;i++)
    {
        ptr[i] = float(rand()%255)/255; //write to buffer
    }


    glUnmapBuffer(GL_ARRAY_BUFFER); //release the mapping to the buffer, invalidating the pointer
    glBindBuffer(GL_ARRAY_BUFFER, 0); //unbinding

}
