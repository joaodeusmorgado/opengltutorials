#version 330

smooth in vec4 f_Color;
//flat in vec4 f_Color;

out vec4 outputColor;

void main()
{
    outputColor = f_Color;
}
