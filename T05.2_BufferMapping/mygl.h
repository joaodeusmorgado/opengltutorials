#ifndef MYGL_H
#define MYGL_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Compatibility>

class MyGL : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Compatibility
{
public:
    MyGL();

protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

public:
    void updateBufferAllData(GLuint &vbo, const float *data, const size_t &dataSize);
    void updateBufferSomeData(GLuint vbo, const float *data, const size_t &dataSize, const int &offset);
    void updateBufferRandomColor(GLuint vbo, const int &offset);

private:
    void initializeVertexBuffer();
    void initializeVao();
    GLuint vbo;
    GLuint vao;

    void createShaders();
    GLuint shaderVert;
    GLuint shaderFrag;
    GLuint program;
};

#endif // MYGL_H
