#include "mygl.h"
#include <QResource>
#include <QFile>
#include <QDebug>
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <sstream>
#include <QtMath>

using namespace std;


MyGL::MyGL() :
    scale(1),
    gl_texture_n(0)
{

}

void MyGL::initializeVertexBuffer()
{

    glGenTextures(1, &textureID);

    glActiveTexture(GL_TEXTURE0+gl_texture_n);
    glBindTexture(GL_TEXTURE_2D, textureID);

    GLint activeT;
    glGetIntegerv(GL_ACTIVE_TEXTURE, &activeT);
    qDebug()<<"GL_ACTIVE_TEXTURE: "<<activeT;
    qDebug()<<"GL_TEXTURE0: "<<GL_TEXTURE0;


    QVector<float> linePattern;
    //linePattern<< 0.0f<<0.0f<<0.0f<< 1.0f<<1.0f<<1.0f;//<< .0<<1.0<<1.0 <<1.0<<1.0<<0.0;
    linePattern<< 1.0f<<0.0f<<0.0f //color 1
               << 1.0f<<1.0f<<1.0f; //color2

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 2, 1, 0,
                 GL_RGB, GL_FLOAT, linePattern.constData());

//    void QOpenGLFunctions::glTexImage2D(GLenum target, GLint level, GLint internalformat, GLsizei width,
  //                                      GLsizei height, GLint border, GLenum format, GLenum type, const GLvoid *pixels)


    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    bool bLinear = false;

    if (bLinear)
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    }
    else
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    }

    glBindTexture(GL_TEXTURE_2D, 0);//unbind
}

void MyGL::createShaders()
{

    //vertex shader----------------------------------------

    QFile fileVertex(":/shaders/shaders/vertshader.vert");
    if (!fileVertex.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"file read fail";
    }
    else
        qDebug()<<"file read ok";

    QTextStream in(&fileVertex);
    QString strShaderVertFile = in.readAll();
    qDebug()<<"strShaderVertFile: "<<strShaderVertFile;
    fileVertex.close();

    shaderVert = glCreateShader(GL_VERTEX_SHADER);

    //const char *strFileData = strShaderFile.toStdString().c_str();//wrong ??? http://stackoverflow.com/questions/11138705/is-there-anything-wrong-with-my-glsl-shader-loading-code
    //glShaderSource(shaderVert, 1, &strFileData, NULL);

    std::string strFileData = strShaderVertFile.toStdString();
    const GLchar *data = reinterpret_cast<const GLchar*>(strFileData.c_str());
    glShaderSource(shaderVert, 1, &data, NULL);

    glCompileShader(shaderVert);

    GLint status;
    glGetShaderiv(shaderVert, GL_COMPILE_STATUS, &status);

    if (status == GL_FALSE)
    {
        qDebug()<<"vertex shader compile fail";
        GLint infoLogLength;
        glGetShaderiv(shaderVert, GL_INFO_LOG_LENGTH, &infoLogLength);
        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetShaderInfoLog(shaderVert, infoLogLength, NULL, strInfoLog);
        const char *strShaderType = "vertex";
        fprintf(stderr, "Compile failure in %s shader:\n%s\n", strShaderType, strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"vertex shader compile ok";
        fprintf(stderr, "Compile vertex shader ok");
    }


    //fragment shader----------------------------------------

    QFile fileFrag(":/shaders/shaders/fragshader.frag");
    if (!fileFrag.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"file read fail";
    }
    else
        qDebug()<<"file read ok";

    QTextStream in2(&fileFrag);
    QString strShaderFragFile = in2.readAll();
    qDebug()<<"strShaderFragFile: "<<strShaderFragFile;
    fileFrag.close();

    shaderFrag = glCreateShader(GL_FRAGMENT_SHADER);

    std::string strFileFragData = strShaderFragFile.toStdString();
    const GLchar *dataFrag = reinterpret_cast<const GLchar*>(strFileFragData.c_str());

    glShaderSource(shaderFrag, 1, &dataFrag, NULL);
    glCompileShader(shaderFrag);

    //GLint status;
    glGetShaderiv(shaderFrag, GL_COMPILE_STATUS, &status);

    if (status == GL_FALSE)
    {
        qDebug()<<"fragment shader compile fail";
        GLint infoLogLength;
        glGetShaderiv(shaderFrag, GL_INFO_LOG_LENGTH, &infoLogLength);
        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetShaderInfoLog(shaderFrag, infoLogLength, NULL, strInfoLog);
        const char *strShaderFragType = "fragment";
        fprintf(stderr, "Compile failure in %s shader:\n%s\n", strShaderFragType, strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"fragment shader compile ok";
        fprintf(stderr, "Compile fragment shader ok");
    }

    //shader program---------------------------------------------------------
    program = glCreateProgram();

    glAttachShader(program, shaderVert);
    glAttachShader(program, shaderFrag);

    glLinkProgram(program);

//    GLint status;
    glGetProgramiv (program, GL_LINK_STATUS, &status);
    if (status == GL_FALSE)
    {
        qDebug()<<"program shader link failled";
        GLint infoLogLength;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);

        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetProgramInfoLog(program, infoLogLength, NULL, strInfoLog);
        fprintf(stderr, "Linker failure: %s\n", strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"program shader link ok";
    }


    glDetachShader(program, shaderVert);
    glDetachShader(program, shaderFrag);


//    return program;
}

void MyGL::initializeGL()
{
    initializeOpenGLFunctions();

    qDebug()<<"initializeGL";
    createShaders();

    //create triangle and buffer data
    initializeVertexBuffer();

    glClearColor(0.5f, 0.5f, 0.5f, 0.0f);
   // glClearColor(0.0f, 0.0f, 1.0f, 0.0f);
    //glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    GLint maxTextures;
    glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &maxTextures);
    qDebug()<<"GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS: "<<maxTextures;

    glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &maxTextures);
    qDebug()<<"GL_MAX_TEXTURE_IMAGE_UNITS: "<<maxTextures;

    glEnable(GL_LINE_SMOOTH);

    GLfloat vals[2];
    glGetFloatv(GL_ALIASED_LINE_WIDTH_RANGE, vals);
    qDebug()<<"GL_ALIASED_LINE_WIDTH_RANGE: "<<vals[0]<<" ; "<<vals[1];

    glGetFloatv(GL_SMOOTH_LINE_WIDTH_RANGE, vals);
    qDebug()<<"GL_SMOOTH_LINE_WIDTH_RANGE: "<<vals[0]<<" ; "<<vals[1];

    glGetFloatv(GL_SMOOTH_LINE_WIDTH_GRANULARITY, vals);
    qDebug()<<"GL_SMOOTH_LINE_WIDTH_GRANULARITY: "<<vals[0]<<" ; "<<vals[1];


    glLineWidth(2.0);


}

void MyGL::resizeGL(int w, int h)
{
    glViewport(0, 0, (GLsizei)w, (GLsizei)h);

    float Range = 2;
    float RangeX = Range;
    float RangeY = Range;
    float RangeZ = Range;

    projection.setToIdentity();
    if (w <= h)
        projection.ortho(-(RangeX),RangeX,-RangeY*h/w,RangeY*h/w,-(RangeZ*2),RangeZ*2);
    else
        projection.ortho(-(RangeX*w/h),RangeX*w/h,-RangeY,RangeY,-(RangeZ*2),RangeZ*2);

}

void MyGL::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glUseProgram(program);

    view.setToIdentity();
    view.scale(scale, scale, scale);
    //view.rotate(55, 0.,1.,.0);

    mvp = projection * view;

    glUniformMatrix4fv(glGetUniformLocation(program , "matrix"), 1, false, projection.constData());
    glUniformMatrix4fv(glGetUniformLocation(program , "mvp"), 1, false, mvp.constData());
    glUniform1i(glGetUniformLocation(program , "outputTexture"), gl_texture_n);

    GLfloat pos = 0.8;
    GLfloat calibration = 2;
    GLfloat st0 = lenght(-pos, pos, -pos, pos) * scale * calibration;
    GLfloat st1 = lenght(pos, pos, -pos, pos) * scale * calibration + st0;
    GLfloat st2 = lenght(-pos, pos, pos, -pos) * scale * calibration + st1;

    GLfloat vertices[] = {
        // Positions        // Texture Coords
        -pos, -pos, 0.0f,   0.0f, 0.0f,   // Bottom left
        pos, pos  , 0.0f,   st0, st0,    // Top right
        -pos, pos, 0.0f,    st1, st1,   // Top left
        pos, -pos  , 0.0f,   st2, st2    // Top Left
      };

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glActiveTexture(GL_TEXTURE0+gl_texture_n);
    glBindTexture(GL_TEXTURE_2D, textureID);

    glVertexAttribPointer(0, 3, GL_FLOAT,GL_FALSE, 5 * sizeof(GLfloat), vertices); //positions start at position 0 in vertices array
    glVertexAttribPointer(1, 2, GL_FLOAT,GL_FALSE, 5 * sizeof(GLfloat), &vertices[3]); //textures coordinates start at position 3 in vertices array

    glDrawArrays(GL_LINE_STRIP, 0, 4);

    glBindTexture(GL_TEXTURE_2D, 0);//unbind
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glUseProgram(0);//release program

}

void MyGL::wheelEvent(QWheelEvent *event)
{
    double minScale = 0.01;
    double maxScale = 20;
    double scaleDelta = 1.25;

    if (event->delta() < 0)
            if (scale > minScale)
                scale /= scaleDelta;

        if (event->delta() > 0)
            if (scale < maxScale)
                scale *= scaleDelta;

        event->accept();
        update();
}

float MyGL::lenght(float a1, float b1, float a2, float b2)
{
    return qSqrt( qPow(b1-a1, 2) + qPow(b2-a2, 2) );
}
