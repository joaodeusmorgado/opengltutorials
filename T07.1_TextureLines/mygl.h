#ifndef MYGL_H
#define MYGL_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QMatrix4x4>
#include <QWheelEvent>

class MyGL : public QOpenGLWidget, protected QOpenGLFunctions
{
public:
    MyGL();


protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();
    void wheelEvent(QWheelEvent *event);

    float lenght(float a1, float b1, float a2, float b2);

private:
    void initializeVertexBuffer();
    GLuint vbos[2];
    GLuint textureID;
    GLint gl_texture_n;
    int imgWidth;
    int imgHeight;
    QMatrix4x4 projection;
    QMatrix4x4 view;
    QMatrix4x4 mvp;
    float scale;

    void createShaders();
    GLuint shaderVert;
    GLuint shaderFrag;
    GLuint program;
};

#endif // MYGL_H
