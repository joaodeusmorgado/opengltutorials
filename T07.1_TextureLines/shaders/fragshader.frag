#version 330

//in vec3 f_Color;
in vec2 TexCoord;

out vec4 outputColor;

uniform sampler2D outputTexture;

void main()
{
    outputColor = texture2D(outputTexture, TexCoord);
}
