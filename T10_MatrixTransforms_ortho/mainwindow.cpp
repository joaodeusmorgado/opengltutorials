#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    xRot = yRot = zRot = 0;
    scale = 1;
    minScale = 0.01;
    maxScale = 80;
    scaleDelta = 1.25;


    gl = new MyGL;
    setCentralWidget(gl);
}

MainWindow::~MainWindow()
{
    delete gl;
    delete ui;
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
        lastPos = event->pos();
}



void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    if (event->buttons()== Qt::LeftButton )
    {

        xRot+=double(event->pos().y()-lastPos.y())/3.6;

        yRot+=double(event->pos().x()-lastPos.x())/3.6;

        zRot+=double(event->pos().x()-lastPos.x())/(3.6*3);
        zRot+=double(event->pos().y()-lastPos.y())/(3.6*3);

        gl->setRotX(xRot);
        gl->setRotY(yRot);
        gl->setRotZ(zRot);

        //updateGL();
        lastPos = event->pos();
        update();
    }
}

void MainWindow::wheelEvent(QWheelEvent * event )
{

    if (event->delta() < 0)
        if (scale > minScale)
            scale /= scaleDelta;

    if (event->delta() > 0)
        if (scale < maxScale)
            scale *= scaleDelta;


    gl->setScale(scale);

    event->accept();
    update();
}

