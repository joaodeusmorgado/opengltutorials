#version 330

//uniform mediump mat4 view;
//uniform mediump mat4 projection;
uniform mat4 mvp;

layout (location = 0) in vec4 position;
layout (location = 1) in vec4 color;

smooth out vec4 f_Color;

void main()
{
    //gl_Position = projection * view * position;
    gl_Position = mvp * position;
    f_Color = color;
}
