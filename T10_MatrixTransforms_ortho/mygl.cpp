#include "mygl.h"
#include <QResource>
#include <QFile>
#include <QDebug>
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <sstream>
using namespace std;


MyGL::MyGL()

{
    xRot = 0;
    yRot = 0;
    zRot = 0;
    scale = scaleX = scaleY = scaleZ = 1;
    tx = 0;
    ty = 0;
    tz = 0;

    range = 5.0f;
}

void MyGL::initializeVertexBuffer()
{


    // 3d axis --------------

    const float vertexData[] = { 
        -range, 0.0f,    0.0f,   1.0f,//axis x-
        1.0f,   0.0f,    0.0f,   1.0f,//color
        range,  0.0f,    0.0f,   1.0f,//axis x+
        1.0f,   0.0f,    0.0f,   1.0f,//color

        0.0f,   -range,  0.0f,   1.0f,//axis y-
        0.0f,   1.0f,    0.0f,   1.0f,//color
        0.0f,   range,   0.0f,   1.0f,//axis y+
        1.0f,   1.0f,    0.0f,   1.0f,//color

        0.0f,   0.0f,    -range, 1.0f,//axis z-
        0.0f,   0.0f,    1.0f,   1.0f,//color
        0.0f,   0.0f,    range,  1.0f,//axis z+
        0.0f,   0.0f,    1.0f,   1.0f//color
    };

    glGenBuffers(1, &positionBufferObject);//create a buffer and save it on the positionBufferObject handle
    glBindBuffer(GL_ARRAY_BUFFER, positionBufferObject); // bind the buffer
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW); //save data to the buffer
    glBindBuffer(GL_ARRAY_BUFFER, 0); //unbinding


    // triangle --------------

    const float triangleData[] = {
         0.0f,    0.5f, 0.5f, 1.0f,//position
         0.5f, -0.366f, 0.5f, 1.0f,//position
        -0.5f, -0.366f, 0.5f, 1.0f,//position
         1.0f,    0.0f, 0.0f, 1.0f,//color
         0.0f,    1.0f, 0.0f, 1.0f,//color
         0.0f,    0.0f, 1.0f, 1.0f,//color
    };

    glGenBuffers(1, &triangleBufferObject);//create a buffer and save it on the positionBufferObject handle
    glBindBuffer(GL_ARRAY_BUFFER, triangleBufferObject); // bind the buffer
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleData), triangleData, GL_STATIC_DRAW); //save data to the buffer
    glBindBuffer(GL_ARRAY_BUFFER, 0); //unbinding

}

void MyGL::createShaders()
{

    //vertex shader----------------------------------------

    QFile fileVertex(":/shaders/shaders/vertshader.vert");
    if (!fileVertex.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"file read fail";
    }
    else
        qDebug()<<"file read ok";

    QTextStream in(&fileVertex);
    QString strShaderVertFile = in.readAll();
    qDebug()<<"strShaderVertFile: "<<strShaderVertFile;
    fileVertex.close();

    shaderVert = glCreateShader(GL_VERTEX_SHADER);

    //const char *strFileData = strShaderFile.toStdString().c_str();//wrong ??? http://stackoverflow.com/questions/11138705/is-there-anything-wrong-with-my-glsl-shader-loading-code
    //glShaderSource(shaderVert, 1, &strFileData, NULL);

    std::string strFileData = strShaderVertFile.toStdString();
    const GLchar *data = reinterpret_cast<const GLchar*>(strFileData.c_str());
    glShaderSource(shaderVert, 1, &data, NULL);

    glCompileShader(shaderVert);

    GLint status;
    glGetShaderiv(shaderVert, GL_COMPILE_STATUS, &status);

    if (status == GL_FALSE)
    {
        qDebug()<<"vertex shader compile fail";
        GLint infoLogLength;
        glGetShaderiv(shaderVert, GL_INFO_LOG_LENGTH, &infoLogLength);
        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetShaderInfoLog(shaderVert, infoLogLength, NULL, strInfoLog);
        const char *strShaderType = "vertex";
        fprintf(stderr, "Compile failure in %s shader:\n%s\n", strShaderType, strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"vertex shader compile ok";
        fprintf(stderr, "Compile vertex shader ok");
    }


    //fragment shader----------------------------------------

    QFile fileFrag(":/shaders/shaders/fragshader.frag");
    if (!fileFrag.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"file read fail";
    }
    else
        qDebug()<<"file read ok";

    QTextStream in2(&fileFrag);
    QString strShaderFragFile = in2.readAll();
    qDebug()<<"strShaderFragFile: "<<strShaderFragFile;
    fileFrag.close();

    shaderFrag = glCreateShader(GL_FRAGMENT_SHADER);

    std::string strFileFragData = strShaderFragFile.toStdString();
    const GLchar *dataFrag = reinterpret_cast<const GLchar*>(strFileFragData.c_str());

    glShaderSource(shaderFrag, 1, &dataFrag, NULL);
    glCompileShader(shaderFrag);

    //GLint status;
    glGetShaderiv(shaderFrag, GL_COMPILE_STATUS, &status);

    if (status == GL_FALSE)
    {
        qDebug()<<"fragment shader compile fail";
        GLint infoLogLength;
        glGetShaderiv(shaderFrag, GL_INFO_LOG_LENGTH, &infoLogLength);
        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetShaderInfoLog(shaderFrag, infoLogLength, NULL, strInfoLog);
        const char *strShaderFragType = "fragment";
        fprintf(stderr, "Compile failure in %s shader:\n%s\n", strShaderFragType, strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"fragment shader compile ok";
        fprintf(stderr, "Compile fragment shader ok");
    }

    //shader program---------------------------------------------------------
    program = glCreateProgram();

    glAttachShader(program, shaderVert);
    glAttachShader(program, shaderFrag);

    glLinkProgram(program);

//    GLint status;
    glGetProgramiv (program, GL_LINK_STATUS, &status);
    if (status == GL_FALSE)
    {
        qDebug()<<"program shader link failled";
        GLint infoLogLength;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);

        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetProgramInfoLog(program, infoLogLength, NULL, strInfoLog);
        fprintf(stderr, "Linker failure: %s\n", strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"program shader link ok";
    }


    glDetachShader(program, shaderVert);
    glDetachShader(program, shaderFrag);


//    return program;
}

void MyGL::initializeGL()
{
    initializeOpenGLFunctions();

    qDebug()<<"initializeGL";
    createShaders();

    //create triangle and buffer data
    initializeVertexBuffer();

    glEnable(GL_DEPTH_TEST);

    glClearColor(0.7f, 0.7f, 0.7f, 0.0f);
}

void MyGL::resizeGL(int w, int h)
{
    projection.setToIdentity();

    range = 1.0f;

    if (w <= h)
        projection.ortho(-(range),range,-range*h/w,range*h/w,-(range*2),range*2);
    else
        projection.ortho(-(range*w/h),range*w/h,-range,range,-(range*2),range*2);

    glViewport(0, 0, (GLsizei)w, (GLsizei)h);
}

void MyGL::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(program);


    view.setToIdentity();
    view.translate( tx, ty, tz );
    view.rotate(xRot, 1.0f, 0.0f, 0.0f );
    view.rotate(yRot, 0.0f, 1.0f, 0.0f );
    view.rotate(zRot, 0.0f, 0.0f, 1.0f );
    view.scale(scale);



    QMatrix4x4 matMVP = projection * view;
    //glUniformMatrix4fv(glGetUniformLocation(program , "view"), 1, false, view.constData());
    //glUniformMatrix4fv(glGetUniformLocation(program , "projection"), 1, false, projection.constData());
    glUniformMatrix4fv(glGetUniformLocation(program , "mvp"), 1, false, matMVP.constData());

    GLsizei stride = 8*sizeof(float);
    //Stride is the distance from the beginning of one index, to the begginging of the following index
    //8 is the number of elements in one index, 4 vertex position elements + 4 vertex color elements

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);


    glBindBuffer(GL_ARRAY_BUFFER, positionBufferObject);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, stride, 0);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, stride, (void*)16); //4*sizeof(float) => 16
    //The offset 16 is the position where color elements start in the array
    //i.e. 4(the number of floats in the vec4 position) * sizeof(float)

    glDrawArrays(GL_LINES, 0, 6);

    // draw triangle
    glBindBuffer(GL_ARRAY_BUFFER, triangleBufferObject);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*)48);
    //The offset 48 is 4 (the size of a float in bytes) * 4 (the number of floats in a vec4) * 3 (the number of vec4's in the vertexData)

    glDrawArrays(GL_TRIANGLES, 0, 3);


    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glUseProgram(0);

}
