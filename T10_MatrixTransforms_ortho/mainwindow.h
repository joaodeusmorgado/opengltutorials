#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMouseEvent>
#include "mygl.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent * event );

    MyGL *gl;

    QPoint lastPos;
    double xRot;
    double yRot;
    double zRot;
    double scale;
    double minScale;
    double maxScale;
    double scaleDelta;

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
