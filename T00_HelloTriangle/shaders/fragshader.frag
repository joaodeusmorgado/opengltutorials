#version 330

uniform vec4 uColor;
out vec4 outputColor;

void main()
{
    outputColor = uColor;
}
