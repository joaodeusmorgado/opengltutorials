#include "mygl.h"
#include <QResource>
#include <QFile>
#include <QDebug>
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <sstream>
using namespace std;


MyGL::MyGL()
{

}

void MyGL::createShaders()
{

    //vertex shader----------------------------------------

    QFile fileVertex(":/shaders/shaders/vertshader.vert");
    if (!fileVertex.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"file read fail";
    }
    else
        qDebug()<<"file read ok";

    QTextStream in(&fileVertex);
    QString strShaderVertFile = in.readAll();
    qDebug()<<"strShaderVertFile: "<<strShaderVertFile;
    fileVertex.close();

    shaderVert = glCreateShader(GL_VERTEX_SHADER);

    //const char *strFileData = strShaderFile.toStdString().c_str();//wrong ??? http://stackoverflow.com/questions/11138705/is-there-anything-wrong-with-my-glsl-shader-loading-code
    //glShaderSource(shaderVert, 1, &strFileData, NULL);

    std::string strFileData = strShaderVertFile.toStdString();
    const GLchar *data = reinterpret_cast<const GLchar*>(strFileData.c_str());
    glShaderSource(shaderVert, 1, &data, NULL);

    glCompileShader(shaderVert);

    GLint status;
    glGetShaderiv(shaderVert, GL_COMPILE_STATUS, &status);

    if (status == GL_FALSE)
    {
        qDebug()<<"vertex shader compile fail";
        GLint infoLogLength;
        glGetShaderiv(shaderVert, GL_INFO_LOG_LENGTH, &infoLogLength);
        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetShaderInfoLog(shaderVert, infoLogLength, NULL, strInfoLog);
        const char *strShaderType = "vertex";
        fprintf(stderr, "Compile failure in %s shader:\n%s\n", strShaderType, strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"vertex shader compile ok";
        fprintf(stderr, "Compile vertex shader ok");
    }


    //fragment shader----------------------------------------

    QFile fileFrag(":/shaders/shaders/fragshader.frag");
    if (!fileFrag.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"file read fail";
    }
    else
        qDebug()<<"file read ok";

    QTextStream in2(&fileFrag);
    QString strShaderFragFile = in2.readAll();
    qDebug()<<"strShaderFragFile: "<<strShaderFragFile;
    fileFrag.close();

    shaderFrag = glCreateShader(GL_FRAGMENT_SHADER);

    std::string strFileFragData = strShaderFragFile.toStdString();
    const GLchar *dataFrag = reinterpret_cast<const GLchar*>(strFileFragData.c_str());

    glShaderSource(shaderFrag, 1, &dataFrag, NULL);
    glCompileShader(shaderFrag);

    //GLint status;
    glGetShaderiv(shaderFrag, GL_COMPILE_STATUS, &status);

    if (status == GL_FALSE)
    {
        qDebug()<<"fragment shader compile fail";
        GLint infoLogLength;
        glGetShaderiv(shaderFrag, GL_INFO_LOG_LENGTH, &infoLogLength);
        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetShaderInfoLog(shaderFrag, infoLogLength, NULL, strInfoLog);
        const char *strShaderFragType = "fragment";
        fprintf(stderr, "Compile failure in %s shader:\n%s\n", strShaderFragType, strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"fragment shader compile ok";
        fprintf(stderr, "Compile fragment shader ok");
    }

    //shader program---------------------------------------------------------
    program = glCreateProgram();

    glAttachShader(program, shaderVert);
    glAttachShader(program, shaderFrag);

    glLinkProgram(program);

//    GLint status;
    glGetProgramiv (program, GL_LINK_STATUS, &status);
    if (status == GL_FALSE)
    {
        qDebug()<<"program shader link failled";
        GLint infoLogLength;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);

        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetProgramInfoLog(program, infoLogLength, NULL, strInfoLog);
        fprintf(stderr, "Linker failure: %s\n", strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"program shader link ok";
    }


    glDetachShader(program, shaderVert);
    glDetachShader(program, shaderFrag);


//    return program;
}

void MyGL::initializeGL()
{
    initializeOpenGLFunctions();

    createShaders();

    glClearColor(0.7f, 0.7f, 0.7f, 0.0f);
}

void MyGL::resizeGL(int w, int h)
{
    glViewport(0, 0, (GLsizei)w, (GLsizei)h);
}

void MyGL::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(program);
    //installs the opengl program object as part of current rendering state

    const float vertexPositions[] = {
        0.0f,    0.5f, 0.0f, 1.0f,//position
        0.5f, -0.366f, 0.0f, 1.0f,//position
       -0.5f, -0.366f, 0.0f, 1.0f,//position
    };

    const float vertexColor[] = {
        0.0f, 0.0f, 1.0f, 1.0f //color
    };

    glEnableVertexAttribArray(0);//enable vertex attribute array

    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, vertexPositions);
    //specify the location and data format of the array of generic vertex attributes
    //to use when rendering

    glUniform4fv(glGetUniformLocation(program , "uColor"), 1, vertexColor);
    //specify the value of a uniform variable for the current program object

    glDrawArrays(GL_TRIANGLES, 0, 3); // let's draw it !!!

    glDisableVertexAttribArray(0);//disable vertex attribute array
    glUseProgram(0);//releases the current opengl program object

}
