#ifndef MYGL_H
#define MYGL_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions_4_1_Compatibility>
//#include <QOpenGLFunctions_4_1_Core>

class MyGL : public QOpenGLWidget, protected QOpenGLFunctions_4_1_Compatibility
//class MyGL : public QOpenGLWidget, protected QOpenGLFunctions_4_1_Core
{
public:
    MyGL();


protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

private:
    void initializeVertexBuffer();
    GLuint vbos[2];

    void createShaders();
    GLuint shaderVert;
    GLuint shaderFrag;
    GLuint program;
};

#endif // MYGL_H
