#include "mainwindow.h"
#include <QApplication>
#include <QSurfaceFormat>
#include <QDebug>

int main(int argc, char *argv[])
{

    //From the Qt documentation:
    //"Note: Calling QSurfaceFormat::setDefaultFormat() before constructing the QApplication instance
    //is mandatory on some platforms (for example, OS X) when an OpenGL core profile context is requested.
    //This is to ensure that resource sharing between contexts stays functional as all internal contexts
    //are created using the correct version and profile."

    QSurfaceFormat format;
    format.setDepthBufferSize(24);
    format.setStencilBufferSize(8);
    format.setVersion(4, 1);
    format.setProfile(QSurfaceFormat::CompatibilityProfile);
    //format.setProfile(QSurfaceFormat::CoreProfile);
    QSurfaceFormat::setDefaultFormat(format);

    QPair<int, int> formatVersion =  format.version();
    qDebug()<<"current default format OpenGL vesion is:"<<formatVersion.first<<","<<formatVersion.second;

    QApplication a(argc, argv);

    MainWindow w;
    w.show();

    return a.exec();
}
