#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "mygl.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    MyGL *gl = new MyGL;

    /*
    QSurfaceFormat format;
    format.setDepthBufferSize(24);
    format.setStencilBufferSize(8);
    format.setVersion(4, 1);
    format.setProfile(QSurfaceFormat::CompatibilityProfile);
    //format.setProfile(QSurfaceFormat::CoreProfile);
    gl->setFormat(format); // must be called before the widget or its parent window gets shown
    */

    setCentralWidget(gl);
}

MainWindow::~MainWindow()
{
    delete ui;
}
