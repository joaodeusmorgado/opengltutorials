#include "mygl.h"
#include <QResource>
#include <QFile>
#include <QDebug>
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <sstream>
using namespace std;


MyGL::MyGL()
{

}

void MyGL::initializeVertexBuffer()
{
    //triangle

    const float vertexPositions[] = {
        0.0f,    0.5f, 0.0f, 1.0f,//position
        0.5f, -0.366f, 0.0f, 1.0f,//position
        -0.5f, -0.366f, 0.0f, 1.0f,//position
    };


    const float vertexColors[] = {
        1.0f,    0.0f, 0.0f, 1.0f,//color
        0.0f,    1.0f, 0.0f, 1.0f,//color
        0.0f,    0.0f, 1.0f, 1.0f,//color
    };

    glGenBuffers(3, vbos);//create a buffer and save it on the vbo handle

    glBindBuffer(GL_ARRAY_BUFFER, vbos[0]); // bind the buffer
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertexPositions), vertexPositions, GL_STATIC_DRAW); //save data to the buffer
    glBindBuffer(GL_ARRAY_BUFFER, 0); //unbinding

    glBindBuffer(GL_ARRAY_BUFFER, vbos[1]); // bind the buffer
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertexColors), vertexColors, GL_STATIC_DRAW); //save data to the buffer
    glBindBuffer(GL_ARRAY_BUFFER, 0); //unbinding


    //triangle
    float offset = -0.5f;
    const float anotherTriangle[] = {
        0.0f+offset,    0.5f+offset, 0.0f, 1.0f,//position
        1.0f,    0.5f, 0.0f, 1.0f,//color
        0.5f+offset, -0.366f+offset, 0.0f, 1.0f,//position
        1.0f,    0.0f, 0.0f, 1.0f,//color
        -0.5f+offset, -0.366f+offset, 0.0f, 1.0f,//position
        1.0f,    1.0f, 0.0f, 1.0f//color
    };

    glBindBuffer(GL_ARRAY_BUFFER, vbos[2]); // bind the buffer
    glBufferData(GL_ARRAY_BUFFER, sizeof(anotherTriangle), anotherTriangle, GL_STATIC_DRAW); //save data to the buffer
    glBindBuffer(GL_ARRAY_BUFFER, 0); //unbinding




}

void MyGL::initializeVao()
{
    //create vao - vertex array object
    glGenVertexArrays(2, vao);

    glBindVertexArray(vao[0]);//vao will save the states of the following calls:
    //glBindBuffer(), glEnableVertexAttribArray() and glVertexAttribPointer()
    //Note: vao does not save the buffer data, only the opengl states

    glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0); //unbind buffer

    glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0); //unbind buffer

    glBindVertexArray(0);//unbind vao


    //bind the second vao
    glBindVertexArray(vao[1]);

    //third triangle
    glBindBuffer(GL_ARRAY_BUFFER, vbos[2]);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    GLsizei stride = 8*sizeof(float);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, stride, 0);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, stride, (void*)16); //4*sizeof(float) => 16
    glBindBuffer(GL_ARRAY_BUFFER, 0); //unbind buffer

    glBindVertexArray(0);//unbind vao


}

void MyGL::createShaders()
{

    //vertex shader----------------------------------------

    QFile fileVertex(":/shaders/shaders/vertshader.vert");
    if (!fileVertex.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"file read fail";
    }
    else
        qDebug()<<"file read ok";

    QTextStream in(&fileVertex);
    QString strShaderVertFile = in.readAll();
    qDebug()<<"strShaderVertFile: "<<strShaderVertFile;
    fileVertex.close();

    shaderVert = glCreateShader(GL_VERTEX_SHADER);

    //const char *strFileData = strShaderFile.toStdString().c_str();//wrong ??? http://stackoverflow.com/questions/11138705/is-there-anything-wrong-with-my-glsl-shader-loading-code
    //glShaderSource(shaderVert, 1, &strFileData, NULL);

    std::string strFileData = strShaderVertFile.toStdString();
    const GLchar *data = reinterpret_cast<const GLchar*>(strFileData.c_str());
    glShaderSource(shaderVert, 1, &data, NULL);

    glCompileShader(shaderVert);

    GLint status;
    glGetShaderiv(shaderVert, GL_COMPILE_STATUS, &status);

    if (status == GL_FALSE)
    {
        qDebug()<<"vertex shader compile fail";
        GLint infoLogLength;
        glGetShaderiv(shaderVert, GL_INFO_LOG_LENGTH, &infoLogLength);
        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetShaderInfoLog(shaderVert, infoLogLength, NULL, strInfoLog);
        const char *strShaderType = "vertex";
        fprintf(stderr, "Compile failure in %s shader:\n%s\n", strShaderType, strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"vertex shader compile ok";
        fprintf(stderr, "Compile vertex shader ok");
    }


    //fragment shader----------------------------------------

    QFile fileFrag(":/shaders/shaders/fragshader.frag");
    if (!fileFrag.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"file read fail";
    }
    else
        qDebug()<<"file read ok";

    QTextStream in2(&fileFrag);
    QString strShaderFragFile = in2.readAll();
    qDebug()<<"strShaderFragFile: "<<strShaderFragFile;
    fileFrag.close();

    shaderFrag = glCreateShader(GL_FRAGMENT_SHADER);

    std::string strFileFragData = strShaderFragFile.toStdString();
    const GLchar *dataFrag = reinterpret_cast<const GLchar*>(strFileFragData.c_str());

    glShaderSource(shaderFrag, 1, &dataFrag, NULL);
    glCompileShader(shaderFrag);

    //GLint status;
    glGetShaderiv(shaderFrag, GL_COMPILE_STATUS, &status);

    if (status == GL_FALSE)
    {
        qDebug()<<"fragment shader compile fail";
        GLint infoLogLength;
        glGetShaderiv(shaderFrag, GL_INFO_LOG_LENGTH, &infoLogLength);
        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetShaderInfoLog(shaderFrag, infoLogLength, NULL, strInfoLog);
        const char *strShaderFragType = "fragment";
        fprintf(stderr, "Compile failure in %s shader:\n%s\n", strShaderFragType, strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"fragment shader compile ok";
        fprintf(stderr, "Compile fragment shader ok");
    }

    //shader program---------------------------------------------------------
    program = glCreateProgram();

    glAttachShader(program, shaderVert);
    glAttachShader(program, shaderFrag);

    glLinkProgram(program);

//    GLint status;
    glGetProgramiv (program, GL_LINK_STATUS, &status);
    if (status == GL_FALSE)
    {
        qDebug()<<"program shader link failled";
        GLint infoLogLength;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);

        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetProgramInfoLog(program, infoLogLength, NULL, strInfoLog);
        fprintf(stderr, "Linker failure: %s\n", strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"program shader link ok";
    }


    glDetachShader(program, shaderVert);
    glDetachShader(program, shaderFrag);


//    return program;
}

void MyGL::initializeGL()
{
    initializeOpenGLFunctions();

    qDebug()<<"initializeGL";
    createShaders();

    //create triangle and buffer data
    initializeVertexBuffer();

    initializeVao();

    glClearColor(0.7f, 0.7f, 0.7f, 0.0f);
}

void MyGL::resizeGL(int w, int h)
{
    glViewport(0, 0, (GLsizei)w, (GLsizei)h);
}

void MyGL::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glUseProgram(program);

    //In this example we use vao[0] for the first and second triangle.
    //We dont need to do any binding for the second triangle, because the vao[0] is already binded,
    //and second triangle is just a position data update, the opengl state doenst change.
    //For the third triangle we bind vao[1] because triangle 3 uses data from another vbo.

    glBindVertexArray(vao[0]);//binding a vao will replace calls to
    //glBindBuffer(), glEnableVertexAttribArray() and glVertexAttribPointer()
    //glBindVertexArray() sets the opengl state machine to the state saved in the initializeVao() function

    float offset = 0.0f;
    float data[] = {
        0.0f+offset, 0.5f+offset, 0.0f, 1.0f,
        0.5f+offset, -0.366f+offset, 0.0f, 1.0f,
        -0.5f+offset, -0.366f+offset, 0.0f, 1.0f
    };
    updateBufferData(vbos[0], data, sizeof(data));

    glDrawArrays(GL_TRIANGLES, 0, 3);//draw triangle 1


    offset = 0.5f;
    float data1[] = {
        0.0f+offset, 0.5f+offset, 0.0f, 1.0f,
        0.5f+offset, -0.366f+offset, 0.0f, 1.0f,
        -0.5f+offset, -0.366f+offset, 0.0f, 1.0f
    };

    updateBufferData(vbos[0], data1, sizeof(data1));
    glDrawArrays(GL_TRIANGLES, 0, 3);//draw triangle 2

    glBindVertexArray(0);//unding previous vao. Probably we would not need to do this call, since the next binding vao will unbind this anyway.


    glBindVertexArray(vao[1]);//bind the second vao
    glDrawArrays(GL_TRIANGLES, 0, 3);//draw triangle 3


    glBindVertexArray(0);// unbing the vao
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glUseProgram(0);//release program

}

void MyGL::updateBufferData(GLuint vbo, const float *data, const int &dataSize)
{

    glBindBuffer(GL_ARRAY_BUFFER, vbo); // bind the buffer
    glBufferSubData(GL_ARRAY_BUFFER, 0, dataSize, data);
    glBindBuffer(GL_ARRAY_BUFFER, 0); //unbinding
}

