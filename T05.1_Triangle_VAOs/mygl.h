#ifndef MYGL_H
#define MYGL_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Compatibility>

class MyGL : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Compatibility
{
public:
    MyGL();

protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

public:
    void updateBufferData(GLuint vbo, const float *data, const int &dataSize);

private:
    void initializeVertexBuffer();
    void initializeVao();
    GLuint vbos[3];
    GLuint vao[2];

    void createShaders();
    GLuint shaderVert;
    GLuint shaderFrag;
    GLuint program;
};

#endif // MYGL_H
