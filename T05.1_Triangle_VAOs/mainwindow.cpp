#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "mygl.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    MyGL *gl = new MyGL;
    QSurfaceFormat format;
    format.setDepthBufferSize(24);
    format.setStencilBufferSize(8);
    format.setVersion(3, 3);
    format.setProfile(QSurfaceFormat::CompatibilityProfile);
    setCentralWidget(gl);
}

MainWindow::~MainWindow()
{
    delete ui;
}
