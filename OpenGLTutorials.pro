    TEMPLATE = subdirs

SUBDIRS += \
    T00_HelloTriangle \
    T01_Triangle_1VBO \
    T02_Triangle_1VBO_Colors \
    T03_TriangleStride \
    T03.1_TriangleStrideNoVbos \
    T04_Triangle_2VBOs \
    T05_Triangle_VAO \
    T05.1_Triangle_VAOs \
    T05.2_BufferMapping \
    T06_Opengl41Format \
    T07_ImageTexture \
    T07.1_TextureLines \
    T07.2_ImageTexture3D \
    T07.3_CubeMap \
    T07.4_ShaderCircle \
    T10_MatrixTransforms_ortho \
    T10.1_MatrixTransforms_perspective \
    T11_FuzzyLine
