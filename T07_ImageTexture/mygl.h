#ifndef MYGL_H
#define MYGL_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>

class MyGL : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT
public:
    MyGL();


public slots:
    void setTexParam(QString value);

signals:
    void texParamChanged(QString newValue);

protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

    void mousePressEvent(QMouseEvent *event);

private:
    void initializeVertexBuffer();
    void setupTexture();
    GLuint vbos[2];
    GLuint textureID;
    GLint gl_texture_n;
    int imgWidth;
    int imgHeight;
    int wrap;
    GLint border;
    QString m_texParam;

    void createShaders();
    GLuint shaderVert;
    GLuint shaderFrag;
    GLuint program;
};

#endif // MYGL_H
