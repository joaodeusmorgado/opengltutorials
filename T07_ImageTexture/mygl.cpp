#include "mygl.h"
#include <QResource>
#include <QFile>
#include <QDebug>
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <sstream>
#include <QMatrix4x4>

using namespace std;


MyGL::MyGL() :
    gl_texture_n(0)
{
    //to use a texture
    //1 - glGenTextures — generate texture names
    //void glGenTextures( GLsizei n, GLuint * textures);

    //2 - glActiveTexture - Specifies which texture unit to make active
    //void glActiveTexture(GLenum texture​);
    //glActiveTexture(GL_TEXTURE0);
    //glActiveTexture(GL_TEXTURE1); or glActiveTexture(GL_TEXTURE0+i);

    //3 - glBindTexture — bind a named texture to a texturing target
    //void glBindTexture( GLenum target, GLuint texture);

    //4 - specify a two-dimensional texture image, or another dimension
    //void glTexImage2D( GLenum target, GLint level, GLint internalFormat, GLsizei width, GLsizei height,
    //                  GLint border, GLenum format, GLenum type, const GLvoid * data);

    //5 - glTexParameter, glTextureParameter — set texture parameters
    //void glTexParameteri(	GLenum target, GLenum pname, GLint param);

}

void MyGL::setTexParam(QString value)
{
    if (value == m_texParam)
        return;
    m_texParam = value;
    emit texParamChanged(value);
}

void MyGL::initializeVertexBuffer()
{


    QImage myImage = QImage(QString(":/images/images/ellipse960_501.png")).mirrored();
    //QImage myImage = QImage(QString(":/images/images/sunset1.png")).mirrored();
    myImage = myImage.convertToFormat(QImage::Format_ARGB32);

    glGenTextures(1, &textureID);//create a texture ID handle

    glActiveTexture(GL_TEXTURE0+gl_texture_n);
    glBindTexture(GL_TEXTURE_2D, textureID); // glBindTexture — bind a named texture to a texturing target
    // generate the texture with the image data

    m_texParam = "GL_REPEAT";
    wrap = 0;
    border = 0;
    imgWidth = myImage.width();
    imgHeight = myImage.height();


    //specify a two-dimensional texture image
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, imgWidth, imgHeight, border,
                 GL_BGRA, GL_UNSIGNED_BYTE, myImage.bits());

    setupTexture();

    glBindTexture(GL_TEXTURE_2D, 0);//unbind

}

void MyGL::setupTexture()
{

    float color[] = { 0.0f, 1.0f, 0.0f, 1.0f }; //border color

    //glTexParameter - set texture parameters
    switch(wrap) {
        case 0:
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
            setTexParam("GL_REPEAT");
            break;
        case 1:
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
            setTexParam("GL_MIRRORED_REPEAT");
            break;
        case 2:
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            setTexParam("GL_CLAMP_TO_EDGE");
            break;
        case 3:
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
            glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, color);
            setTexParam("GL_CLAMP_TO_BORDER");
            //glTexCoord — set the current texture coordinates
            //glTexCoord2f(-0.5/imgWidth,-0.5/imgHeight); // For a 64x64 texture
            //glTexCoord2f(1+0.5/imgWidth,1+0.5/imgHeight);
            break;
        case 4:
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
            setTexParam("GL_CLAMP");
            break;

    }

    bool bLinear = true;
    if (bLinear)
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    }
    else
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    }

    glGenerateMipmap(GL_TEXTURE_2D);

}

void MyGL::createShaders()
{

    //vertex shader----------------------------------------

    QFile fileVertex(":/shaders/shaders/vertshader.vert");
    if (!fileVertex.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"file read fail";
    }
    else
        qDebug()<<"file read ok";

    QTextStream in(&fileVertex);
    QString strShaderVertFile = in.readAll();
    qDebug()<<"strShaderVertFile: "<<strShaderVertFile;
    fileVertex.close();

    shaderVert = glCreateShader(GL_VERTEX_SHADER);

    //const char *strFileData = strShaderFile.toStdString().c_str();//wrong ??? http://stackoverflow.com/questions/11138705/is-there-anything-wrong-with-my-glsl-shader-loading-code
    //glShaderSource(shaderVert, 1, &strFileData, NULL);

    std::string strFileData = strShaderVertFile.toStdString();
    const GLchar *data = reinterpret_cast<const GLchar*>(strFileData.c_str());
    glShaderSource(shaderVert, 1, &data, NULL);

    glCompileShader(shaderVert);

    GLint status;
    glGetShaderiv(shaderVert, GL_COMPILE_STATUS, &status);

    if (status == GL_FALSE)
    {
        qDebug()<<"vertex shader compile fail";
        GLint infoLogLength;
        glGetShaderiv(shaderVert, GL_INFO_LOG_LENGTH, &infoLogLength);
        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetShaderInfoLog(shaderVert, infoLogLength, NULL, strInfoLog);
        const char *strShaderType = "vertex";
        fprintf(stderr, "Compile failure in %s shader:\n%s\n", strShaderType, strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"vertex shader compile ok";
        fprintf(stderr, "Compile vertex shader ok");
    }


    //fragment shader----------------------------------------

    QFile fileFrag(":/shaders/shaders/fragshader.frag");
    if (!fileFrag.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"file read fail";
    }
    else
        qDebug()<<"file read ok";

    QTextStream in2(&fileFrag);
    QString strShaderFragFile = in2.readAll();
    qDebug()<<"strShaderFragFile: "<<strShaderFragFile;
    fileFrag.close();

    shaderFrag = glCreateShader(GL_FRAGMENT_SHADER);

    std::string strFileFragData = strShaderFragFile.toStdString();
    const GLchar *dataFrag = reinterpret_cast<const GLchar*>(strFileFragData.c_str());

    glShaderSource(shaderFrag, 1, &dataFrag, NULL);
    glCompileShader(shaderFrag);

    //GLint status;
    glGetShaderiv(shaderFrag, GL_COMPILE_STATUS, &status);

    if (status == GL_FALSE)
    {
        qDebug()<<"fragment shader compile fail";
        GLint infoLogLength;
        glGetShaderiv(shaderFrag, GL_INFO_LOG_LENGTH, &infoLogLength);
        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetShaderInfoLog(shaderFrag, infoLogLength, NULL, strInfoLog);
        const char *strShaderFragType = "fragment";
        fprintf(stderr, "Compile failure in %s shader:\n%s\n", strShaderFragType, strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"fragment shader compile ok";
        fprintf(stderr, "Compile fragment shader ok");
    }

    //shader program---------------------------------------------------------
    program = glCreateProgram();

    glAttachShader(program, shaderVert);
    glAttachShader(program, shaderFrag);

    glLinkProgram(program);

//    GLint status;
    glGetProgramiv (program, GL_LINK_STATUS, &status);
    if (status == GL_FALSE)
    {
        qDebug()<<"program shader link failled";
        GLint infoLogLength;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);

        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetProgramInfoLog(program, infoLogLength, NULL, strInfoLog);
        fprintf(stderr, "Linker failure: %s\n", strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"program shader link ok";
    }


    glDetachShader(program, shaderVert);
    glDetachShader(program, shaderFrag);


//    return program;
}

void MyGL::initializeGL()
{
    initializeOpenGLFunctions();

    qDebug()<<"initializeGL";
    createShaders();

    //create triangle and buffer data
    initializeVertexBuffer();

    //glClearColor(0.7f, 0.7f, 0.7f, 0.0f);
    glClearColor(0.0f, 0.0f, 1.0f, 0.0f);
}

void MyGL::resizeGL(int w, int h)
{
    glViewport(0, 0, (GLsizei)w, (GLsizei)h);
}

void MyGL::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glUseProgram(program);



  /*  float Range = 2;
    //float RangeX = imgWidth;
    //float RangeY = imgHeight;
    float RangeX = Range;// * imgHeight/imgWidth;
    float RangeY = Range;
    float RangeZ = Range;
    int w = width();
    int h = height();

    QMatrix4x4 projection;
    projection.setToIdentity();
    if (w <= h)
        projection.ortho(-(RangeX),RangeX,-RangeY*h/w,RangeY*h/w,-(RangeZ*2),RangeZ*2);
    else
        projection.ortho(-(RangeX*w/h),RangeX*w/h,-RangeY,RangeY,-(RangeZ*2),RangeZ*2);

    glUniformMatrix4fv(glGetUniformLocation(program , "matrix"), 1, false, projection.constData());
*/

    //this will add a rgb tone to the image
    GLfloat r = 0.0f;
    GLfloat g = 1.0f;
    GLfloat b = 1.0f;

    GLfloat pos = 0.8;

    GLfloat si = 0;
    GLfloat sf = 8;
    GLfloat ti = 0;
    GLfloat tf = 8;
    GLfloat vertices[] = {
        // Positions    // Colors  // Texture Coords
        -pos, -pos, 0.0f,  r, g, b,   si, ti,   // Bottom Left
        -pos,  pos, 0.0f,  r, g, b,   si, tf,    // Top Left
        pos, -pos, 0.0f,   r, g, b,   sf, ti,   // Bottom Right
        pos,  pos, 0.0f,   r, g, b,   sf, tf   // Top Right
    };


    glActiveTexture(GL_TEXTURE0+gl_texture_n);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glUniform1i(glGetUniformLocation(program , "outputTexture"), gl_texture_n);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    glVertexAttribPointer(0, 3, GL_FLOAT,GL_FALSE, 8 * sizeof(GLfloat), vertices); //positions start at position 0 in vertices array
    glVertexAttribPointer(1, 3, GL_FLOAT,GL_FALSE, 8 * sizeof(GLfloat), &vertices[3]); //colors start at position 3 in vertices array
    glVertexAttribPointer(2, 2, GL_FLOAT,GL_FALSE, 8 * sizeof(GLfloat), &vertices[6]); //textures coordinates start at position 6 in vertices array

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    //glDrawArrays(GL_LINES, 0, 4);

    glBindTexture(GL_TEXTURE_2D, 0);//unbind
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
    glUseProgram(0);//release program

}

void MyGL::mousePressEvent(QMouseEvent *event)
{
    Q_UNUSED(event);

    glBindTexture(GL_TEXTURE_2D, textureID); // glBindTexture — bind a named texture to a texturing target

    wrap++;
    if (wrap > 4)
        wrap = 0;

    setupTexture();

    glBindTexture(GL_TEXTURE_2D, 0);//unbind

    update();
}
