#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QGridLayout>
#include <QLabel>
#include "mygl.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    MyGL *gl = new MyGL;


    QGridLayout *gridLayout = new QGridLayout(this->centralWidget());
    QLabel *label = new QLabel(parent);

    gridLayout->setSpacing(6);
    gridLayout->setContentsMargins(11, 11, 11, 11);
    gridLayout->setObjectName(QStringLiteral("gridLayout"));

    label->setObjectName(QStringLiteral("teste"));
    label->setText("GL_REPEAT");
    QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
    sizePolicy.setHorizontalStretch(0);
    sizePolicy.setVerticalStretch(0);
    sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
    label->setSizePolicy(sizePolicy);

    gridLayout->addWidget(label, 0, 0, 1, 1);

    gl->setObjectName(QStringLiteral("openGLWidget"));
    QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
    sizePolicy1.setHorizontalStretch(0);
    sizePolicy1.setVerticalStretch(0);
    sizePolicy1.setHeightForWidth(gl->sizePolicy().hasHeightForWidth());
    gl->setSizePolicy(sizePolicy1);
    gridLayout->addWidget(gl, 1, 0, 1, 1);

    connect(gl, &MyGL::texParamChanged, label, &QLabel::setText );
    //connect(gl, SIGNAL(texParamChanged(QString)), label, SLOT(setText(QString)));
}

MainWindow::~MainWindow()
{
    delete ui;
}
