#-------------------------------------------------
#
# Project created by QtCreator 2016-01-16T16:57:52
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mygl.cpp \
    mycamera.cpp

HEADERS  += mainwindow.h \
    mygl.h \
    mycamera.h

FORMS    += mainwindow.ui

RESOURCES += \
    resource.qrc
