#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QGridLayout>
#include <QLabel>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    xRot = yRot = zRot = 0;
    scale = 1;
    minScale = 0.01;
    maxScale = 80;
    scaleDelta = 1.25;

    gl = new MyGL;

    QGridLayout *gridLayout = new QGridLayout(this->centralWidget());
    QLabel *label = new QLabel(parent);

    gridLayout->setSpacing(6);
    gridLayout->setContentsMargins(11, 11, 11, 11);
    gridLayout->setObjectName(QStringLiteral("gridLayout"));

    label->setObjectName(QStringLiteral("teste"));
    label->setText("GL_REPEAT");
    QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
    sizePolicy.setHorizontalStretch(0);
    sizePolicy.setVerticalStretch(0);
    sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
    label->setSizePolicy(sizePolicy);

    gridLayout->addWidget(label, 0, 0, 1, 1);

    gl->setObjectName(QStringLiteral("openGLWidget"));
    QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
    sizePolicy1.setHorizontalStretch(0);
    sizePolicy1.setVerticalStretch(0);
    sizePolicy1.setHeightForWidth(gl->sizePolicy().hasHeightForWidth());
    gl->setSizePolicy(sizePolicy1);
    gridLayout->addWidget(gl, 1, 0, 1, 1);

    connect(gl, &MyGL::texParamChanged, label, &QLabel::setText );
    //connect(gl, SIGNAL(texParamChanged(QString)), label, SLOT(setText(QString)));
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
        lastPos = event->pos();
}


void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    if (event->buttons()== Qt::LeftButton )
    {

        xRot+=double(event->pos().y()-lastPos.y())/3.6;

        yRot+=double(event->pos().x()-lastPos.x())/3.6;

        zRot+=double(event->pos().x()-lastPos.x())/(3.6*3);
        zRot+=double(event->pos().y()-lastPos.y())/(3.6*3);

        gl->setRotX(xRot);
        gl->setRotY(yRot);
        gl->setRotZ(zRot);

        //updateGL();
        lastPos = event->pos();
        update();
    }
}

void MainWindow::wheelEvent(QWheelEvent * event )
{

    if (event->delta() < 0)
        if (scale > minScale)
            scale /= scaleDelta;

    if (event->delta() > 0)
        if (scale < maxScale)
            scale *= scaleDelta;


    gl->setScale(scale);

    event->accept();
    update();
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    float deltaX = 0.1;
    float deltaY = 0.1;

    if (event->key() == Qt::Key_Up)
        gl->CamXForward(deltaX);
    if (event->key() == Qt::Key_Down)
        gl->CamXBackward(deltaX);

    if (event->key() == Qt::Key_Right)
        gl->CamXForward(deltaY);
    if (event->key() == Qt::Key_Left)
        gl->CamXBackward(deltaY);


}

