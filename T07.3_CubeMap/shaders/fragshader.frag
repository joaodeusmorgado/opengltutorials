#version 330
#extension GL_NV_shadow_samplers_cube : enable

//smooth in vec4 f_Color;
//flat in vec4 f_Color;
in vec3 f_Color;

in vec3 TexCoord;

out vec4 outputColor;

uniform samplerCube cubeTexture;
//uniform sampler2D outputTexture;
uniform bool useTextures;

void main()
{
    if (useTextures)
        outputColor = textureCube(cubeTexture, TexCoord);// * vec4(f_Color, 1.0f);
    else
        outputColor = vec4(f_Color, 1.0f);
    //outputColor = texture2D(outputTexture, TexCoord) * f_Color;// + f_Color;
    //outputColor = texture2D(outputTexture, TexCoord);
}
