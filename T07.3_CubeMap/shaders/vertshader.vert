#version 330

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;
layout (location = 2) in vec3 texCoord;
uniform mat4 mvp;

out vec3 f_Color;
out vec3 TexCoord;

void main()
{
    gl_Position = mvp * vec4(position, 1.0f);
    //gl_Position = vec4(position, 1.0f);
    f_Color = color;
    TexCoord = texCoord;
}
