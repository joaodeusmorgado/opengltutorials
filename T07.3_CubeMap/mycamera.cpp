#include "mycamera.h"
#include <QQuaternion>

MyCamera::MyCamera() :
    m_eye(-1,-1,1),
    m_center(1,1,1),
    m_up(0,0,1)
{
    m_camera.lookAt(m_eye, m_center, m_up);

}

void MyCamera::setEye(const QVector3D eye_)
{
    m_eye = eye_;
}

void MyCamera::setCenter(const QVector3D center_)
{
    m_center = center_;
}

void MyCamera::rotateUp_ClockWise()
{
    QQuaternion q;
    q.fromDirection(m_center-m_eye, m_up);
}

void MyCamera::rotateUp_CounterClockWise()
{

}

void MyCamera::resetUp()
{
    m_up = QVector3D(0,0,1);
}
