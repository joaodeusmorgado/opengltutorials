#ifndef MYGL_H
#define MYGL_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QMatrix4x4>

class MyGL : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT
public:
    MyGL();

    void setRotX(const double &x){xRot = x; update();}
    void setRotY(const double &y){yRot = y; update();}
    void setRotZ(const double &z){zRot = z; update();}
    void setScale(const double &scale_){scale = scale_; update();}

    void CamXForward(float x){eye.setX(eye.x()+x); update();}
    void CamXBackward(float x){eye.setX(eye.x()-x); update();};
    void CamYForward(float y){eye.setY(eye.y()+y); update();};
    void CamYBackward(float y){eye.setY(eye.y()-y); update();};

public slots:
    void setTexParam(QString value);

signals:
    void texParamChanged(QString newValue);

protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

private:
    void initializeVertexBuffer();
    void setupTexture();
    GLuint positionBufferObject;
    GLuint textureID;
    GLint gl_texture_n;
    int imgWidth;
    int imgHeight;
    QMatrix4x4 projection;
    QMatrix4x4 view;
    QMatrix4x4 mvp;
    float scale;
    float xRot;
    float yRot;
    float zRot;
    QVector3D eye;

    int wrap;
    GLint border;
    QString m_texParam;

    void createShaders();
    GLuint shaderVert;
    GLuint shaderFrag;
    GLuint program;
};

#endif // MYGL_H
