#ifndef MYCAMERA_H
#define MYCAMERA_H

#include <QVector3D>
#include <QMatrix4x4>

class MyCamera
{
public:
    MyCamera();

    void setEye(const QVector3D eye_);
    void setCenter(const QVector3D center_);

    void rotateUp_ClockWise();
    void rotateUp_CounterClockWise();
    void resetUp(); //to (0,0,1)

private:
    QVector3D m_eye;
    QVector3D m_center;
    QVector3D m_up;
    QMatrix4x4 m_camera;

};

#endif // MYCAMERA_H
