#ifndef MYGL_H
#define MYGL_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>

class MyGL : public QOpenGLWidget, protected QOpenGLFunctions
{
public:
    MyGL();


protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

private:
    void initializeVertexBuffer();
    GLuint positionBufferObject;

    void createShaders();
    GLuint shaderVert;
    GLuint shaderFrag;
    GLuint program;
};

#endif // MYGL_H
