#ifndef MYGL_H
#define MYGL_H

#include <QOpenGLWidget>
#include <QMatrix4x4>
#include <QOpenGLFunctions>

class MyGL : public QOpenGLWidget, protected QOpenGLFunctions
{
public:
    MyGL();

    void setRotX(const double &x){xRot = x; update();}
    void setRotY(const double &y){yRot = y; update();}
    void setRotZ(const double &z){zRot = z; update();}
    void setScale(const double &scale_){scale = scale_; update();}


protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

    QMatrix4x4 projection;
    QMatrix4x4 view;
    double scale;
    double scaleX;
    double scaleY;
    double scaleZ;
    double xRot;
    double yRot;
    double zRot;
    double tx;
    double ty;
    double tz;


private:
    float range;
    void initializeVertexBuffer();
    GLuint positionBufferObject;
    GLuint triangleBufferObject;

    void createShaders();
    GLuint shaderVert;
    GLuint shaderFrag;
    GLuint program;
};

#endif // MYGL_H
