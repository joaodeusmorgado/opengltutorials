#version 330

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texCoord;
uniform mat4 matrix;
uniform mat4 mvp;

out vec2 TexCoord;

void main()
{
    gl_Position = mvp * vec4(position, 1.0f);
    //gl_Position = vec4(position, 1.0f);
    TexCoord = texCoord;
}
