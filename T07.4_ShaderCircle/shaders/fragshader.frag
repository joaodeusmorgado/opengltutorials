#version 330

//in vec3 f_Color;
in vec2 TexCoord;
uniform vec4 color;
out vec4 outputColor;
uniform sampler2D outputTexture;

uniform int selection;
uniform float circleRadius;
uniform vec2 circleCenterXY;
uniform float circleCenterX;
uniform float circleCenterY;
const float threshold = 0.005;
uniform float u_time;
uniform vec2 u_mouse;
uniform vec2 u_resolution;

uniform float PI = 3.14159265359;

/*
//subroutine---------------------------------------------

//subroutine signature
subroutine vec4 circle();

//option1 - normal circle
subroutine (circle) vec4 normalCircle()
{
    return vec4(1.0, 0.0, 0.0, 1.0);
}

//option 2 - dot circle
subroutine (circle) vec4 dotCircle()
{
    return vec4(0.0, 0.0, 1.0, 1.0);
}

//subroutine selector - to be set on the application side
subroutine uniform circle circleTypeSelector;

//subroutine---------------------------------------------
*/


// selection == 1
vec4 circle()
{
    float delta = 0.5f;
    if ( distance(circleCenterXY, gl_FragCoord.xy)  < circleRadius-delta)
        discard;
    else if( distance(circleCenterXY, gl_FragCoord.xy ) > circleRadius+delta)
        discard;
    else
        return outputColor = vec4(1.,0.,0.,1.);
}


vec4 circleFollowingMouse()
{
    // circle following the mouse
    vec4 colorDot = vec4(1.0, 0.0, 0.0, 1.0);
    vec4 colorBackGround = vec4(0.0, 0.0, 0.0, 1.0);

    float circleWidth = 1.0;
    float radius = 25.0;
    float dist = distance(u_mouse, gl_FragCoord.st);
    if ( distance(dist, radius) <  circleWidth )
        //return outputColor = mix(colorDot, colorBackGround, 0.5);
        return outputColor = colorDot;
    else
        return outputColor = colorBackGround;
    // circle following the mouse
}

vec4 fuzzyDot()
{
    // fuzzy red dot following the mouse
    vec4 colorDot = vec4(1.0, 0.0, 0.0, 1.0);
    vec4 colorBackGround = vec4(0.0, 0.0, 0.0, 1.0);

    float radius = 55.0;
    float dist = distance(u_mouse, gl_FragCoord.st);
    if ( dist < radius )
        return outputColor = mix(colorDot, colorBackGround, dist/radius);
    return outputColor = colorBackGround;
}

vec4 mouseCross()
{
    //draw a red green cross following the mouse in the black background
    float dist = 1.0;
    vec4 color1 =  vec4(1.0, 0.0, 0.0, 1.0);
    vec4 color2 =  vec4(1.0, 1.0, 0.0, 1.0);

    if ( abs(gl_FragCoord.x - u_mouse.x) < dist)
        return outputColor = color1;

    if ( abs(gl_FragCoord.y - u_mouse.y) < dist )
        return outputColor = color2;

    //discard;
    return outputColor = vec4(0.0, 0.0, 0.0, 1.0);
}

vec4 circleDot()
{
    vec2 st = gl_FragCoord.xy/u_resolution;

    float delta = 1.0f;
    if ( distance(circleCenterXY, gl_FragCoord.xy)  < circleRadius-delta)
        discard;

    if( distance(circleCenterXY, gl_FragCoord.xy ) > circleRadius+delta)
        discard;


    float angle = 0.0;
    if (st.x-0.5 != 0)
        angle = atan(st.x-0.5, st.y-0.5);
    else
        angle = 0.0;

    if ( (angle) < PI*0.25 )
        return outputColor = vec4(1.,0.,0.,1.);
    else
        discard;
    //return outputColor = mix(colorDot, colorBackGround, dist/radius);
}

vec4 line()
{
    return outputColor = color;
}


float plot(vec2 st, float pct)
{
    return  smoothstep( pct-0.02, pct, st.y)
            - smoothstep( pct, pct+0.02, st.y);
}

vec4 shapingFunctions()
{
    vec2 st = gl_FragCoord.xy/u_resolution;

    //float y = st.x;
    float y = sin(3*PI*st.x);

    vec3 color = vec3(y);

    float pct = plot(st,y);
    color = (1.0-pct)*color+pct*vec3(0.0,1.0,0.0);
    //color = pct*vec3(0.0,1.0,0.0);
    //color = (1.0-pct)*color;

    return outputColor = vec4(color, 1.0);

  //  vec2 st = gl_FragCoord.xy/u_resolution;
    //return outputColor = vec4(st, 0.0, 1.0);
}

vec4 drawLine()
{
    vec2 st = gl_FragCoord.xy/u_resolution;
    vec3 color = vec3(0.0, 0.0, 0.0);

    float delta = 0.01;
    if (abs(pow(st.x, 5) - st.y) < delta)
    {
        color = vec3(1.0, 0.0, 0.0);
    }
    else {
        discard;
    }

        //color = mix(vec3(1.0, 0.0, 0.0), vec3(0.0, 0.0, 0.0), 0.5);
    return outputColor = vec4(color, 1.0);
}


void main()
{

    if (selection == 1)
        circle();
    else if (selection == 2)
        circleFollowingMouse();
    else if (selection == 3)
        fuzzyDot();
    else if (selection == 4)
        mouseCross();
    else if (selection == 5)
        circleDot();
    else if (selection == 6)
        shapingFunctions();
    else if (selection == 7)
        line();
    else if (selection == 8)
        drawLine();
    else
        outputColor = color; //vec4(0.0, 1.0, 0.0, 1.0);

    //vec2 st = gl_FragCoord.xy/u_resolution;
    //outputColor = vec4(st.x,st.y,0.0,1.0);

    //float angle = atan( gl_FragCoord.y - circleCenterY , gl_FragCoord.x - circleCenterX );

    /*
    float f1 = 0.002;
    float f2 = 0.0002;
    float f3 = 0.001;
    outputColor = vec4(abs(sin(f1*u_time)),
                            abs(sin(f2*u_time-2.1)),
                            abs(sin(f3*u_time-3.4)),1.0);
*/


    /*
    vec2 aCirclePosition = vec2(circleCenterX, circleCenterY);
    float dist = distance(aCirclePosition, gl_FragCoord.xy);

    if (dist == 0.)
        dist = 1.;

    float d = circleRadius / dist;

    if (d >= 1.)
        outputColor = vec4(0.8, 0.0, 0.0, 0.0); //texture2D(outputTexture, TexCoord);
    else if (d >= 1. - threshold)
        outputColor = vec4(0.0, 0.0, 0.9, 0.0);  //texture2D(outputTexture, TexCoord);
    else
        outputColor = vec4(0.0, 0.5, 0.0, 0.0);
    */
}

//--------------------------------------------------------------------------------------
