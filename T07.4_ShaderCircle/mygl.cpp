#include "mygl.h"
#include <QResource>
#include <QFile>
#include <QDebug>
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <sstream>
#include <QtMath>

using namespace std;


MyGL::MyGL() :
    gl_texture_n(0),
    scale(1),
    m_mouseX(0.0), m_mouseY(0.0),
    Range(10),
    selection(3)
{
    m_resolution.resize(2);

}

void MyGL::initializeVertexBuffer()
{

    glGenTextures(1, &textureID);

    glActiveTexture(GL_TEXTURE0+gl_texture_n);
    glBindTexture(GL_TEXTURE_2D, textureID);

    GLint activeT;
    glGetIntegerv(GL_ACTIVE_TEXTURE, &activeT);
    qDebug()<<"GL_ACTIVE_TEXTURE: "<<activeT;
    qDebug()<<"GL_TEXTURE0: "<<GL_TEXTURE0;


    QVector<float> linePattern;
    linePattern<< 0.0f<<0.0f<<0.0f<< 1.0f<<1.0f<<1.0f;//<< .0<<1.0<<1.0 <<1.0<<1.0<<0.0;
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 2, 1, 0,
                 GL_RGB, GL_FLOAT, linePattern.constData());


    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    bool bLinear = false;

    if (bLinear)
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    }
    else
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    }

    glBindTexture(GL_TEXTURE_2D, 0);//unbind
}

void MyGL::createShaders()
{

    //vertex shader----------------------------------------

    QFile fileVertex(":/shaders/shaders/vertshader.vert");
    if (!fileVertex.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"file read fail";
    }
    else
        qDebug()<<"file read ok";

    QTextStream in(&fileVertex);
    QString strShaderVertFile = in.readAll();
    qDebug()<<"strShaderVertFile: "<<strShaderVertFile;
    fileVertex.close();

    shaderVert = glCreateShader(GL_VERTEX_SHADER);

    //const char *strFileData = strShaderFile.toStdString().c_str();//wrong ??? http://stackoverflow.com/questions/11138705/is-there-anything-wrong-with-my-glsl-shader-loading-code
    //glShaderSource(shaderVert, 1, &strFileData, NULL);

    std::string strFileData = strShaderVertFile.toStdString();
    const GLchar *data = reinterpret_cast<const GLchar*>(strFileData.c_str());
    glShaderSource(shaderVert, 1, &data, NULL);

    glCompileShader(shaderVert);

    GLint status;
    glGetShaderiv(shaderVert, GL_COMPILE_STATUS, &status);

    if (status == GL_FALSE)
    {
        qDebug()<<"vertex shader compile fail";
        GLint infoLogLength;
        glGetShaderiv(shaderVert, GL_INFO_LOG_LENGTH, &infoLogLength);
        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetShaderInfoLog(shaderVert, infoLogLength, NULL, strInfoLog);
        const char *strShaderType = "vertex";
        fprintf(stderr, "Compile failure in %s shader:\n%s\n", strShaderType, strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"vertex shader compile ok";
        fprintf(stderr, "Compile vertex shader ok");
    }


    //fragment shader----------------------------------------

    QFile fileFrag(":/shaders/shaders/fragshader.frag");
    if (!fileFrag.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"file read fail";
    }
    else
        qDebug()<<"file read ok";

    QTextStream in2(&fileFrag);
    QString strShaderFragFile = in2.readAll();
    qDebug()<<"strShaderFragFile: "<<strShaderFragFile;
    fileFrag.close();

    shaderFrag = glCreateShader(GL_FRAGMENT_SHADER);

    std::string strFileFragData = strShaderFragFile.toStdString();
    const GLchar *dataFrag = reinterpret_cast<const GLchar*>(strFileFragData.c_str());

    glShaderSource(shaderFrag, 1, &dataFrag, NULL);
    glCompileShader(shaderFrag);

    //GLint status;
    glGetShaderiv(shaderFrag, GL_COMPILE_STATUS, &status);

    if (status == GL_FALSE)
    {
        qDebug()<<"fragment shader compile fail";
        GLint infoLogLength;
        glGetShaderiv(shaderFrag, GL_INFO_LOG_LENGTH, &infoLogLength);
        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetShaderInfoLog(shaderFrag, infoLogLength, NULL, strInfoLog);
        const char *strShaderFragType = "fragment";
        fprintf(stderr, "Compile failure in %s shader:\n%s\n", strShaderFragType, strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"fragment shader compile ok";
        fprintf(stderr, "Compile fragment shader ok");
    }

    //shader program---------------------------------------------------------
    program = glCreateProgram();

    glAttachShader(program, shaderVert);
    glAttachShader(program, shaderFrag);

    glLinkProgram(program);

//    GLint status;
    glGetProgramiv (program, GL_LINK_STATUS, &status);
    if (status == GL_FALSE)
    {
        qDebug()<<"program shader link failled";
        GLint infoLogLength;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);

        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetProgramInfoLog(program, infoLogLength, NULL, strInfoLog);
        fprintf(stderr, "Linker failure: %s\n", strInfoLog);
        delete[] strInfoLog;
    }
    else
    {
        qDebug()<<"program shader link ok";
    }


    glDetachShader(program, shaderVert);
    glDetachShader(program, shaderFrag);


//    return program;
}

void MyGL::initializeGL()
{
    initializeOpenGLFunctions();

    qDebug()<<"initializeGL";
    createShaders();

    //create triangle and buffer data
    initializeVertexBuffer();

    glClearColor(0.5f, 0.5f, 0.5f, 0.0f);
   // glClearColor(0.0f, 0.0f, 1.0f, 0.0f);
    //glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    GLint maxTextures;
    glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &maxTextures);
    qDebug()<<"GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS: "<<maxTextures;

    glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &maxTextures);
    qDebug()<<"GL_MAX_TEXTURE_IMAGE_UNITS: "<<maxTextures;

    //glEnable(GL_LINE_SMOOTH);

    setMouseTracking(true);
    m_time.start();
}

void MyGL::resizeGL(int w, int h)
{
    m_width = float(w);
    m_height = float(h);
    m_resolution.insert(0,float(w));
    m_resolution.insert(1,float(h));


    glViewport(0, 0, (GLsizei)w, (GLsizei)h);

    float RangeX = Range;
    float RangeY = Range;
    float RangeZ = Range;

    projection.setToIdentity();
    if (w <= h)
        projection.ortho(-(RangeX),RangeX,-RangeY*h/w,RangeY*h/w,-(RangeZ*2),RangeZ*2);
    else
        projection.ortho(-(RangeX*w/h),RangeX*w/h,-RangeY,RangeY,-(RangeZ*2),RangeZ*2);
}

void MyGL::mouseMoveEvent(QMouseEvent *ev)
{
    m_mouseX = float( ev->x() );
    m_mouseY = float( ev->y() );

    update();

    //qDebug()<<"mouseX: "<<m_mouseX;
    //qDebug()<<"mouseY: "<<m_mouseY;
}

void MyGL::mouseDoubleClickEvent(QMouseEvent *ev)
{
    Q_UNUSED(ev);

    selection++;
    if (selection == 9)
        selection = 1;
    qDebug()<<"selection: "<<selection;
    update();
}

void MyGL::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glUseProgram(program);

    view.setToIdentity();
    view.scale(scale*0.8, scale*0.8, scale*0.6);
    view.rotate(30, 0.8, 0.4, 1.0);

    mvp = projection * view;

    GLfloat pos = Range;

    float t = m_time.elapsed();
    qDebug()<<"time elapsed: "<<t*0.002;

    qDebug()<<"width: "<<m_width;
    qDebug()<<"height: "<<m_height;


    glUniform4f(glGetUniformLocation(program , "color"), 0.0, 1.0, 0.0, 1.0); // default background color
    glUniform1f(glGetUniformLocation(program , "circleRadius"), 50.);
    glUniform1f(glGetUniformLocation(program , "circleCenterX"), 0.0f); //replace with something equivalent to vec2 for centerXY
    glUniform1f(glGetUniformLocation(program , "circleCenterY"), 0.0f);
    glUniform1f(glGetUniformLocation(program , "u_time"), t);
    glUniform2f(glGetUniformLocation(program , "u_resolution"), m_width, m_height);
    glUniform2f(glGetUniformLocation(program , "u_mouse"), m_mouseX, m_height - m_mouseY);
    glUniform2f(glGetUniformLocation(program , "circleCenterXY"), m_width*0.5, m_height*0.5);

    glUniformMatrix4fv(glGetUniformLocation(program , "matrix"), 1, false, projection.constData());
    glUniformMatrix4fv(glGetUniformLocation(program , "mvp"), 1, false, mvp.constData());
    glUniform1i(glGetUniformLocation(program , "outputTexture"), gl_texture_n);
    //GLint selection;
    //selection = 3;
    glUniform1i(glGetUniformLocation(program, "selection"), selection);

    /*GLfloat pos = 0.8;
    GLfloat calibration = 1;
    GLfloat st0 = lenght(-pos, pos, -pos, pos) * scale * calibration;
    GLfloat st1 = lenght(pos, pos, -pos, pos) * scale * calibration + st0;
    GLfloat st2 = lenght(-pos, pos, pos, -pos) * scale * calibration + st1;

    GLfloat vertices[] = {
        // Positions        // Texture Coords
        -pos, -pos, 0.0f,   0.0f, 0.0f,   // Bottom left
        pos, pos  , 0.0f,   st0, st0,    // Top right
        -pos, pos, 0.0f,    st1, st1,   // Top left
        pos, -pos  , 0.0f,   st2, st2    // Top Left
      };*/


    GLfloat calibration = 3;
    GLfloat dist = lenght(-pos, pos, pos, pos);
    GLfloat st0 = dist * scale * calibration;
    GLfloat st1 = dist * scale * calibration + st0;
    GLfloat st2 = dist * scale * calibration + st1;
    //GLfloat st3 = dist * scale * calibration + st2;

    GLfloat vertices[] = {
        // Positions        // Texture Coords
        -pos, -pos, 0.0f,   0.0f, 0.0f,   // bottom left
        -pos, pos, 0.0f,   st0, st0,    // Top left
        pos, -pos, 0.0f,    st1, st1,    // Bottom Right
        pos, pos, 0.0f,   st2, st2  // top right
      };



    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glActiveTexture(GL_TEXTURE0+gl_texture_n);
    glBindTexture(GL_TEXTURE_2D, textureID);

    glVertexAttribPointer(0, 3, GL_FLOAT,GL_FALSE, 5 * sizeof(GLfloat), vertices); //positions start at position 0 in vertices array
    glVertexAttribPointer(1, 2, GL_FLOAT,GL_FALSE, 5 * sizeof(GLfloat), &vertices[3]); //textures coordinates start at position 3 in vertices array


    //selection = 0;glUniform1i(glGetUniformLocation(program, "selection"), selection);
    //glDrawArrays(GL_LINES, 0, 4);

    //selection = 5;glUniform1i(glGetUniformLocation(program, "selection"), selection);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    /*

    float lineWidth=0.1;
    GLfloat line[] = {
        // Positions
        -pos*1.3f, -lineWidth*0.5f, 0.0f,
        -pos*1.3f, lineWidth*0.5f, 0.0f,
        pos*1.3f, -lineWidth*0.5f, 0.0f,
        pos*1.3f, lineWidth*0.5f, 0.0f
      };

    glVertexAttribPointer(0, 3, GL_FLOAT,GL_FALSE, 3 * sizeof(GLfloat), line);
    glUniform4f(glGetUniformLocation(program , "color"), 1.0, 1.0, 0.0, 1.0); // default background color

    selection = 7;glUniform1i(glGetUniformLocation(program, "selection"), selection);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
*/

    glBindTexture(GL_TEXTURE_2D, 0);//unbind
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glUseProgram(0);//release program

    //update();
}

void MyGL::wheelEvent(QWheelEvent *event)
{
    double minScale = 0.01;
    double maxScale = 20;
    double scaleDelta = 1.25;

    if (event->delta() < 0)
            if (scale > minScale)
                scale /= scaleDelta;

        if (event->delta() > 0)
            if (scale < maxScale)
                scale *= scaleDelta;

        event->accept();
        //update();
}

float MyGL::lenght(float a1, float b1, float a2, float b2)
{
    return qSqrt( qPow(b1-a1, 2) + qPow(b2-a2, 2) );
}
