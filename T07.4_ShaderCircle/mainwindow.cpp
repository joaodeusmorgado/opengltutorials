#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "mygl.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    MyGL *gl = new MyGL;
    setCentralWidget(gl);
}

MainWindow::~MainWindow()
{
    delete ui;
}
