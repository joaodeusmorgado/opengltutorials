#ifndef MYGL_H
#define MYGL_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QMatrix4x4>
#include <QWheelEvent>
#include <QTime>
//#include <QMouseEvent>

class MyGL : public QOpenGLWidget, protected QOpenGLFunctions
{
public:
    MyGL();


protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void mouseMoveEvent(QMouseEvent *ev);
    void mouseDoubleClickEvent(QMouseEvent *ev);
    void paintGL();
    void wheelEvent(QWheelEvent *event);

    float lenght(float a1, float b1, float a2, float b2);

private:
    void initializeVertexBuffer();
    GLuint vbos[2];
    GLuint textureID;
    GLint gl_texture_n;
    int imgWidth;
    int imgHeight;
    QMatrix4x4 projection;
    QMatrix4x4 view;
    QMatrix4x4 mvp;
    float scale;
    QVector<float> m_resolution;
    float m_width;
    float m_height;
    float Range;
    float m_mouseX;
    float m_mouseY;

    void createShaders();
    GLuint shaderVert;
    GLuint shaderFrag;
    GLuint program;
    QTime m_time;
    GLint selection;
};

#endif // MYGL_H
